package main

import (
	"context"
	"errors"
	"os"

	"gitlab.com/lucyfire/godl/pkg/cmd/root"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

func main() {
	f := cmdutil.NewFactory()
	f.CleanupHandler()
	cmd := root.NewCmdRoot(f)
	err := cmd.ExecuteContext(f.Context.Ctx)
	if err == nil {
		err = f.Context.Ctx.Err()
	}
	var exitCode int
	switch {
	case err == nil:
		exitCode = 0
	case errors.Is(err, context.Canceled):
		exitCode = 130
	default:
		exitCode = 1
	}
	f.Context.Wait()
	os.Exit(exitCode)
}
