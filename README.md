# Godl
[![Latest Release](https://gitlab.com/lucyfire/godl/-/badges/release.svg)](https://gitlab.com/lucyfire/godl/-/releases)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/lucyfire/godl)](https://goreportcard.com/report/gitlab.com/lucyfire/godl)

# Description

Godl is a tool that runs some pre-defined operations using these tools:
- [youtube-dl (yt-dlp)](https://github.com/yt-dlp/yt-dlp)
- [ffmpeg](https://ffmpeg.org/ffmpeg.html)
- [eyeD3](https://eyed3.readthedocs.io)
- [spotify-downloader](https://github.com/spotDL/spotify-downloader)

# Installation
- Download one of the released binaries and add it to your PATH.
- Build the binary yourself using `task build:build`

# Usage
- Use as `godl (command) {arguments} [flags]`.
- Checkout `gold completion` for terminal autocompletion options

---

# Command Definitions
[Commands](#commands)
- [Audio](#godl-audio)
  - [Chop](#godl-audio-chop)
  - [Metadata](#godl-audio-metadata)
  - Thumbnail
    - [Add](#godl-audio-thumbnail-add)
    - [Extract](#godl-audio-thumbnail-extract)
    - [Remove](#godl-audio-thumbnail-remove)
- [Combine](#godl-combine)
- [Convert](#godl-convert)
- [Slice](#godl-slice)
- [Spotify](#godl-spotify)
- [Video](#godl-video)
  - [Concat](#godl-video-concat)
  - [Crop](#godl-video-crop)
  - [Cropv2](#godl-video-cropv2)
  - [Encode](#godl-video-encode)
  - [Extract-Audio](#godl-video-extract-audio)
  - [Remove-Audio](#godl-video-remove-audio)
  - [Remove-Duplicate-Frames](#godl-video-remove-duplicate-frames)


# Commands
## Global Flags:
```  -d, --daemon               Run command in daemon
      --debug                Displays verbose information
      --help                 Show help for command
      --ignore-validations   Ignore command validations
```


### godl audio

Downloads url as mp3

Usage:
```bash
godl audio { <url> } [flags]
$ godl audio https://www.youtube.com/watch?v=dQw4w9WgXcQ [ --keep-thumbnail | --output output-name ]
```

Flags:
```
  -f, --format string    The format to download. (Defaults to the best audio)
      --keep-thumbnail   Keep thumbnail as a separate file
      --list-formats     Lists the available formats of the url
  -o, --output string    The output filename. (Defaults to the page title)
```


---


#### godl audio chop

Used to chop a big audio file into smaller files, used to chop a whole music
album into its corresponding tracks. Can also optionally embed a thumbnail to
all chopped files. 


Usage:
```bash
godl audio chop { <audio-file> } [flags]
$ godl audio chop full-album-file.mp3 --metadata metadata.json [ --thumbnail thumbnail.jpg ]
```

Flags:
```
      --ignore-file-validations   Ignore any file type validations
  -m, --metadata string           A json file containing metadata
                                  Example:
                                  {
                                  	"artist": "Mephorash",
                                  	"genre": "Black Metal",
                                  	"album": "Rites of Nullification",
                                  	"totalTracks": 4,
                                  	"year": 2015,
                                  	"tracks": [
                                  		{
                                  			"trackNumber": 1,
                                  			"from": "00:00:00",
                                  			"to": "00:09:28",
                                  			"title": "Riphyon - The Tree of Assiyah Putrescent",
                                  			"fileName": "Riphyon - The Tree of Assiyah Putrescent"
                                  		},
                                  		{
                                  			"trackNumber": 2,
                                  			"from": "00:09:29",
                                  			"to": "00:20:11",
                                  			"title": "Phezur - Dissolving the Sea of Yetzirah",
                                  			"fileName": "Phezur - Dissolving the Sea of Yetzirah"
                                  		},
                                  		{
                                  			"trackNumber": 3,
                                  			"from": "00:20:12",
                                  			"to": "00:30:53",
                                  			"title": "Cheidolun - Breaking the Blade of Beriah",
                                  			"fileName": "Cheidolun - Breaking the Blade of Beriah"
                                  		},
                                  		{
                                  			"trackNumber": 4,
                                  			"from": "00:30:54",
                                  			"to": "00:42:39",
                                  			"title": "Berberioth - Vandalising the Throne of Atziluth",
                                  			"fileName": "Berberioth - Vandalising the Throne of Atziluth"
                                  		}
                                  	]
                                  }
  -t, --thumbnail string          The thumbnail to add to sub-files
```


---


#### godl audio metadata

Adds at least 1 metadata option to an audio file

Usage:
```bash
godl audio metadata { <audio-file> } [flags]
$ godl audio metadata audio-file.mp3 \
	--thumbnail thumbnail.jpg \
	--artist Mephorash \
	--title "Riphyon - The Tree of Assiyah Putrescent" \
	--album "Rites of Nullification" \
	--year 2015 \
	--genre "Black Metal" \
	--track-number 1 \
	--track-total 4 \
	--lyrics lyrics-file.lrc \
	[ --ignore-file-validations ]


```

Flags:
```
      --album string              Album's name
      --artist string             Artist name
      --genre string              Song's genre
      --ignore-file-validations   Ignore any file type validations
      --lyrics-file string        A lyrics file
  -t, --thumbnail string          The thumbnail to add to sub-files
      --title string              Title name
      --track-number int          TrackNumber's number on the album (use 0 to clear) (default -1)
      --track-total-number int    Total number of tracks on the album (use 0 to clear) (default -1)
      --year int                  Song's release year
```


---


---


##### godl audio thumbnail add

Adds thumbnail to an audio file

Usage:
```bash
godl audio thumbnail add { <audio-file> } [flags]
$ godl audio thumbnail add audiofile.mp3 --thumbnail thumbnail.jpg [ --ignore-file-validations ]
```

Flags:
```
      --ignore-file-validations   Ignore any file type validations
  -t, --thumbnail string          The thumbnail
```


---


##### godl audio thumbnail extract

Extracts all images from an audio file to current directory

Usage:
```bash
godl audio thumbnail extract { <audio-filepath> } [flags]
$ godl audio thumbnail extract audiofile.mp3 [ --ignore-file-validations ]
```

Flags:
```
      --ignore-file-validations   Ignore any file type validations
```


---


##### godl audio thumbnail remove

Removes all images from an audio file

Usage:
```bash
godl audio thumbnail remove { <image-filepath> } [flags]
$ godl audio thumbnail remove audiofile.mp3 [ --write-images | --ignore-file-validations ]
```

Flags:
```
      --ignore-file-validations   Ignore any file type validations
      --write-images              Write all images to current directory before removing
```


---


### godl combine

Combines a video and audio file into a new file. The audio will override all
existing audio in the video file. Outputs combined file keeping the extension of
the video file.


Usage:
```bash
godl combine [flags]
$ godl combine --video video-file.mp4 --audio audio-file.mp3 [ --output output-name | --ignore-file-validations ]
```

Flags:
```
  -a, --audio string              The audio file
      --ignore-file-validations   Ignore any file type validations
  -o, --output string             The output filename
  -v, --video string              The video file
```


---


### godl convert

Re encodes a file using ffmpeg into another format, you will be prompted with
valid formats depended on the input file.


Usage:
```bash
godl convert { <audio-file> | <video-file> } [flags]
$ godl convert { video-file.mp4 | audio-file.mp3 } [ --output output-name ]
```

Flags:
```
      --ignore-file-validations   Ignore any file type validations
  -o, --output string             The output filename
```


---


### godl slice

Slices an audio/video file from the specified time

Usage:
```bash
godl slice { <audio-file> | <video-file> } [flags]
$ godl slice { video-file.mp4 | audio-file.mp3 } --from 00:00:00.0 --to 00:00:10.5 [--output output-name | --ignore-file-validations ]
```

Flags:
```
      --from Time                 slice start time (Format: {HH:MM:SS[.MS]}) (default 00:00:00.000)
      --ignore-file-validations   Ignore any file type validations
  -o, --output string             The output filename
      --to Time                   slice finish time (Format: {HH:MM:SS[.MS]}) (default 00:00:00.000)
```


---


### godl spotify

Downloads metadata from spotify and the music file from youtube

Usage:
```bash
godl spotify <spotify-url> [flags]
$ godl spotify https://open.spotify.com/track/4cOdK2wGLETKBW3PvgPWqT
```


---


### godl video

Downloads url

Usage:
```bash
godl video { <url> } [flags]
$ godl video https://www.youtube.com/watch?v=dQw4w9WgXcQ [ --keep-thumbnail | --output output-name ]
```

Flags:
```
  -f, --format string    The format to download. (Defaults to the best video and best audio)
      --keep-thumbnail   Keep thumbnail as a separate file
      --list-formats     Lists the available formats of the url
  -o, --output string    The output filename. (Defaults to the page title)
```


---


#### godl video concat

Concatenates multiple videos into a single file, the new file will have
the max width/height across all the videos combined. Final file is re-encoded
to x264 in the same container as the first video.


Usage:
```bash
godl video concat [flags]
$ godl concat video-1.mp4 video-2.mp4 video-3.mp4

```

Flags:
```
      --docker                    Run command in docker (default true)
      --ignore-file-validations   Ignore any file type validations
  -o, --output string             The output filename
```


---


#### godl video crop

Crops an area of width x height starting from position X x Y

Usage:
```bash
godl video crop { <video-file> } [flags]
# Crops a 100 by 100 pixels starting from top left
$ godl video crop video-file.mp4 \ 
	--width  100 --height  100 \
	--x-position 0 --y-position 0 \
	[ --output output-name | --ignore-file-validations ]

```

Flags:
```
  -h, --height int                The height of the cropped area
      --ignore-file-validations   Ignore any file type validations
      --interactive               Visually choose the crop area
  -o, --output string             The output filename
  -w, --width int                 The width of the cropped area
  -x, --x-position int            The x position the crop area will start
  -y, --y-position int            The y position the crop area will start
```


---


#### godl video cropv2

Crops an area of width x height starting from position X x Y

Usage:
```bash
godl video cropv2 { <video-file> } [flags]
# Crops a 100 by 100 pixels starting from top left
$ godl video cropv2 video-file.mp4 \ 
	--width  100 --height  100 \
	--x-position 0 --y-position 0 \
	[ --output output-name | --ignore-file-validations ]
# Interactive way to crop video
$ godl video cropv2 video-file.mp4 --interactive 

```

Flags:
```
      --docker                    Run command in docker (default true)
  -h, --height int                The height of the cropped area
      --ignore-file-validations   Ignore any file type validations
      --interactive               interactive way to run the command, usually spawns a tui
  -o, --output string             The output filename
  -w, --width int                 The width of the cropped area
  -x, --x-position int            The x position the crop area will start
  -y, --y-position int            The y position the crop area will start
```


---


#### godl video encode

Re-encodes a video changing its [constant rate factor](https://trac.ffmpeg.org/wiki/Encode/H.264)
and [video encoders](https://www.ffmpeg.org/ffmpeg-codecs.html#Video-Encoders)
values resulting in a smaller or bigger file depending on selected values.


Usage:
```bash
godl video encode { <video-file> } [flags]
$ godl video encode video-file.mp4 --vcoded H.265 [ --crf 23 | --output output-name ]
```

Flags:
```
      --crf int                   The range of the CRF scale is 0–51, where 0 is lossless, 23 is the default, and 51 is worst quality possible.
                                  A lower value generally leads to higher quality, and a subjectively sane range is 17–28.
                                  Consider 17 or 18 to be visually lossless or nearly so; it should look the same or nearly the same as the input but it isn't technically lossless.
                                  The range is exponential, so increasing the CRF value +6 results in roughly half the bitrate / file size, while -6 leads to roughly twice the bitrate.
                                   (default 23)
      --ignore-file-validations   Ignore any file type validations
  -o, --output string             The output filename
      --vcodec VideoCodec         the video codec. Options: [H.264, H.265] (default H.264)
```


---


#### godl video extract-audio

Extracts the first audio channel from a video file into mp3

Usage:
```bash
godl video extract-audio { <video-file> } [flags]
$ godl video extract-audio video-file.mp4 [ --output output-name | --ignore-file-validations ]
```

Flags:
```
  -f, --format AudioMimeType      The output format (default Mp3)
      --ignore-file-validations   Ignore any file type validations
  -o, --output string             The output filename
```


---


#### godl video remove-audio

Removes all audio channels from a video file

Usage:
```bash
godl video remove-audio { <video-file> } [flags]
$ godl video remove-audio video-file.mp4 [ --output output-name | --ignore-file-validations ]
```

Flags:
```
      --ignore-file-validations   Ignore any file type validations
  -o, --output string             The output filename
```


---


#### godl video remove-duplicate-frames

Removes duplicated frames from a video file

Usage:
```bash
godl video remove-duplicate-frames { <video-file> } [flags]
$ godl video remove-duplicate-frames video-file.mp4 [ --output output-name | --ignore-file-validations ]
```

Flags:
```
      --ignore-file-validations   Ignore any file type validations
  -o, --output string             The output filename
```


---

