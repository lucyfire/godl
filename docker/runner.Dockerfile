FROM golang:1.23-alpine

RUN apk add --no-cache git curl\
    && go install github.com/go-task/task/v3/cmd/task@latest \
    && go install github.com/mgechev/revive@latest \
    && go install golang.org/x/tools/cmd/goimports@latest \
    && go install github.com/essentialkaos/aligo/v2@latest

