FROM golang:1.23-alpine AS build

WORKDIR /root

RUN cd /root && \
    go install github.com/go-task/task/v3/cmd/task@latest

COPY . .

RUN task build:build

FROM python:3.11-alpine3.18

ARG YT_DLP_VERSION
ARG SPOT_DL_VERSION

ENV GODL_RUNNING_IN_DOCKER=true
ENV TERM=xterm-kitty

COPY --from=build /root/build/godl /usr/bin/godl

RUN apk update && \
    pip3 install --no-cache-dir --upgrade pip

RUN pip3 install --upgrade --no-cache-dir eyeD3 && \
    apk --no-cache add ffmpeg

RUN adduser -D godluser

RUN pip3 install --no-cache-dir -Iv yt-dlp==$YT_DLP_VERSION spotdl==$SPOT_DL_VERSION

USER godluser
WORKDIR /home/godluser
