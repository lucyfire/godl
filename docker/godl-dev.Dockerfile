FROM registry.gitlab.com/lucyfire/godl/godl:dev2
COPY godl /usr/bin/godl

ENV GODL_RUNNING_IN_DOCKER=true
ENV TERM=xterm-kitty

USER godluser
WORKDIR /home/godluser
