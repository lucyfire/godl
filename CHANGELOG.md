# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [4.4.0] - 2024-10-17

### Added
- Video cropv2 command Added

### Changed
- Updated all packages to latest
- Updated to go 1.23
- Added notice to video crop command that it's gonna be replaced with cropv2
  in v5.0.0

## [4.3.0] - 2024-10-01

### Added
- windows binary in new releases

### Changed
- Dropped Makefile in favor of taskfile.yml
- Dropped cosmtrek/air in favor of taskfile.yml

### Removed
- Removed `syscall.SIGKILL` and `syscall.SIGTSTP` from the caught signals

## [4.2.0] - 2024-08-14
### New features
- `godl video crop` interactive mode

### Under the hood new/changed features
- Updated all packages to latest
- update to go 1.22

### Removed features
- `godl docker`

## [4.0.0] - 2023-09-01

### New features
- New global flag `--ignore-validations` ignores all validations of a command.

### Changed features
- Changed flag `--ignore-validations` to `--ignore-file-validations` since it
  just skips validations associated to files.
- Changed slice command to use `fps_mode` over the deprecated `vsync` in ffmpeg. 

### Under the hood new/changed features
- (#7) Allow running multiple commands inside the same container with [Docker Wrapper](https://gitlab.com/lucyfire/docker)
  library
- Updated all packages to latest, docker sdk builds correctly with go1.20.6
- Added tests to validate the cli commands
- Always resolve filepaths to their full path
- update to go 1.21

## [3.0.1] - 2023-07-29

### Deprecated
- Added deprecation notice for `--os` flag

## [3.0.0] - 2023-07-26

### New features
- New command lists running docker containers with options `godl docker [flags]`
- Added weekly ci job that checks for new yt-dlp version and auto bumps godl 
version.
- Experimental feature to attach all docker containers to stdout 1 line each
`godl docker --show-output`
- New flag `--ignore-validations` to ignore file type validations when 
performing operations, this is mainly to combat audio files encoded in video
containers.
- New spotify command, that uses [spotify-downloader](https://github.com/spotDL/spotify-downloader/tree/master)
downloads metadata from spotify and music from youtube
- New flag `--lyrics-file` for `godl audio metadata` embeds the lyrics in the
audio file

### Changed features
- Changed flag of running in daemon from `--silent|-s` to `--daemon|-d`
- Changed capitalized commands to dashed
  - `video removeAudio` to `video remove-audio`
  - `video extractAudio` to `video extract-audio`
  - `video removeDuplicateFrames` to `remove-duplicate-frames`
- Changed image name from `registry.gitlab.com/lucyfire/godl/youtube_dl` to
`registry.gitlab.com/lucyfire/godl/godl`

### Removed features
- Removed m3u8 download command
- Removed update command, now each released binary is tied to its own docker tag

### Under the hood new/changed features
- All docker containers spawned by `godl` now have a label `creator=godl`
- Autogenerate README file from the commands.
- Converted all docker execution from running os docker commands to using the
docker SDK.

### Bug fixes
- Can now properly type in dockers containers to answer prompted questions (#5)

## [2.2.0] - 2023-05-26
### New features
- Introduced `CHANGELOG`
- New video command `extract audio` from a video
    ```bash
    godl video extractAudio <video-file>
    ```
- New video command `crop` crops an area from a video
    ```bash
    godl video crop <video-file> --width 100 --height 100 --x-position 0 --y-position 50
    ```  
- Added linters

### Changed features
- Reduced the number of build commands in the makefile by using more variables
- Updated gitlab ci/cd to run linters and tests as well as to accommodate the
new makefile changes

### Bug fixes
- Fix slice command always producing audio files #1

### Removed
- Removed `godl version` command in favor of `godl -v`

## [2.1.1] - 2023-04-03
### New features
- New video command `remove duplicate frames` from a video
    ```bash
    godl video removeDuplicateFrames <video-file>
    ```
- Releases for `Macos arm64`, `Linux amd64` and `Raspberry Pi arm64` 

## [2.0.1] - 2023-04-03
### Major changes
- Migrate from `github.com/urfave/cli/v2` to `spf13/cobra` implementation

### Added
- Add watch build using `cosmtrek/air` for ease of dev checking
 
### Changed features
- Lower docker image size by using alpine base image
- Significantly updated flags and argument checks as well as error messages 
- Restructured the whole codebase
- Update `yt-dlp` from `2022.4.8` to `2023.3.4`
- Update `go` from `1.16` to `1.20`

## [1.0.0] - 2022-05-22
- Initial release implementation with `github.com/urfave/cli/v2`
