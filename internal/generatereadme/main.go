package main

import (
	"fmt"
	"os"
	"strings"
	"text/template"

	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/godl/pkg/cmd/root"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"golang.org/x/text/cases"
	"golang.org/x/text/language"
)

func main() {
	generateReadme(root.NewCmdRoot(&cmdutil.Factory{}))
}

type tmplData struct {
	MainCommand tmplCommand
	Commands    []tmplCommand
}

type tmplCommand struct {
	Name        string
	Description string
	Usage       string
	Examples    string
	CommandName string
	MenuItem    string
	Flags       string
	Runnable    bool
}

func generateReadme(r *cobra.Command) {
	atom := zap.NewAtomicLevel()
	cfg := zap.NewProductionEncoderConfig()
	cfg.TimeKey = ""
	cfg.EncodeLevel = zapcore.CapitalColorLevelEncoder
	logger := zap.New(zapcore.NewCore(zapcore.NewConsoleEncoder(cfg), zapcore.Lock(os.Stdout), atom))
	tmpl, err := template.ParseFiles("README.go.tmpl")
	if err != nil {
		logger.Fatal("Failed to parse readme template", zap.Error(err))
	}
	outputFile, err := os.Create("README.md")
	if err != nil {
		logger.Fatal("Failed to create README.md file", zap.Error(err))
	}
	defer outputFile.Close()
	data := tmplData{
		MainCommand: tmplCommand{
			Name:        r.Name(),
			CommandName: r.CommandPath(),
			Flags:       r.PersistentFlags().FlagUsages(),
		},
		Commands: generateReadmeRecursively([]*cobra.Command{r}, 0),
	}
	err = tmpl.Execute(outputFile, data)
	if err != nil {
		logger.Fatal("Failed to render the template", zap.Error(err))
	}
}

func generateReadmeRecursively(cmds []*cobra.Command, level int) (commands []tmplCommand) {
	for _, c := range cmds {
		if c.Hidden {
			continue
		}
		if level > 0 {
			cmd := tmplCommand{
				Name:        cases.Title(language.English, cases.Compact).String(c.Name()),
				Description: c.Short,
				CommandName: fmt.Sprintf("%s %s", strings.Repeat("#", level+2), c.CommandPath()),
				Flags:       c.LocalFlags().FlagUsages(),
				Runnable:    c.Runnable(),
				Usage:       c.UseLine(),
				Examples:    c.Example,
			}
			if c.Long != "" {
				cmd.Description = c.Long
			}
			if c.Runnable() {
				cmd.MenuItem = fmt.Sprintf("%s- [%s](#%s)\n",
					strings.Repeat("  ", level-1),
					cases.Title(language.English, cases.Compact).String(c.Name()),
					strings.Join(strings.Split(c.CommandPath(), " "), "-"),
				)
			} else {
				cmd.MenuItem = fmt.Sprintf("%s- %s\n",
					strings.Repeat("  ", level-1),
					cases.Title(language.English, cases.Compact).String(c.Name()),
				)
			}
			commands = append(commands, cmd)
		}
		if c.HasSubCommands() {
			commands = append(commands, generateReadmeRecursively(c.Commands(), level+1)...)
		}
	}
	return commands
}
