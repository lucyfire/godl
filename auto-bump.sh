#!/usr/bin/bash
GODL_VERSION=$(git describe --abbrev=0 --tags --match "v*.*.*" --exclude "*-rc*" --exclude "*-alpha*")
echo "Current Godl version: $GODL_VERSION"
NEW_GODL_VERSION=$(echo "$GODL_VERSION" | awk -F. '{print $1"."$2"."$3+1}')
echo "New Godl version: $NEW_GODL_VERSION"
MSG="version bump"

CURRENT_YTDLP_VERSION=$(grep -m 1 "YT_DLP_VERSION" "taskfile.yml" | sed 's/^.*: //g')
NEW_YTDLP_VERSION=$(curl -s https://pypi.org/pypi/yt-dlp/json | grep -o 'version":"\d*\.\d*\.\d*' | sed s/version\"\:\"//g)
echo "Current yt-dlp version: $CURRENT_YTDLP_VERSION"
echo "New yt-dlp version: $NEW_YTDLP_VERSION"
if [ "$CURRENT_YTDLP_VERSION" != "$NEW_YTDLP_VERSION" ]; then
	sed -i "./taskfile.yml" -e "s/YT_DLP_VERSION: $CURRENT_YTDLP_VERSION/YT_DLP_VERSION: $NEW_YTDLP_VERSION/g"
	MSG="${MSG} yt-dlp: $CURRENT_YTDLP_VERSION => $NEW_YTDLP_VERSION"
fi

CURRENT_SPOTDL_VERSION=$(grep -m 1 "SPOT_DL_VERSION" "taskfile.yml" | sed 's/^.*: //g')
NEW_SPOTDL_VERSION=$(curl -s https://pypi.org/pypi/spotdl/json | grep -o 'version":"\d*\.\d*\.\d*' | sed s/version\"\:\"//g)
echo "Current spotdl version: $CURRENT_SPOTDL_VERSION"
echo "New spotdl version: $NEW_SPOTDL_VERSION"
if [ "$CURRENT_SPOTDL_VERSION" != "$NEW_SPOTDL_VERSION" ]; then
	sed -i "./taskfile.yml" -e "s/SPOT_DL_VERSION: $CURRENT_SPOTDL_VERSION/SPOT_DL_VERSION: $NEW_SPOTDL_VERSION/g"
	MSG="${MSG} spotdl: $CURRENT_SPOTDL_VERSION => $NEW_SPOTDL_VERSION"
fi

if [ "$MSG" != "version bump" ]; then
	git config user.name "$GITLAB_USER_NAME"
	git config user.email "$GITLAB_USER_EMAIL"
	git remote set-url origin "$CI_REPOSITORY_URL"
	git add taskfile.yml
	git commit -m ":arrow_up: $MSG"
	git push "https://$GITLAB_USER_LOGIN:$GITLAB_ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git" "HEAD:$CI_COMMIT_REF_NAME"
	git tag "$NEW_GODL_VERSION"
	git push "https://$GITLAB_USER_LOGIN:$GITLAB_ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git" "$NEW_GODL_VERSION"
fi
