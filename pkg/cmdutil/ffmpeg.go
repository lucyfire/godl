package cmdutil

import (
	"bufio"
	"io"
	"strconv"
	"strings"
	"time"
)

type FfmpegProgressState string

const (
	FfmpegProgressContinue FfmpegProgressState = "continue"
	FfmpegProgressEnd      FfmpegProgressState = "end"
)

type FfmpegProgress struct {
	StartTime          time.Time
	BitRate            string
	Speed              string
	Progress           FfmpegProgressState
	Duration           time.Duration
	Frame              int
	TotalSize          int
	OutTimeMiliSeconds int
	DupFrames          int
	DroppedFrames      int
	FPS                float32
}

// ParseFfmpegProgress parses the ffmpeg progress output and returns a channel
// of FfmpegProgress.
//
// NOTE: When the ffmpeg process has multiple files as input, the progress
// duration restarts at 0 after each file.
func ParseFfmpegProgress(r io.Reader) <-chan *FfmpegProgress {
	ch := make(chan *FfmpegProgress)
	go func() {
		progress := new(FfmpegProgress)
		progress.StartTime = time.Now()
		scanner := bufio.NewScanner(r)
		for scanner.Scan() {
			t := scanner.Text()
			parts := strings.SplitN(t, "=", 2)
			if len(parts) != 2 {
				continue
			}
			atoi, _ := strconv.Atoi(parts[1])
			atof, _ := strconv.ParseFloat(parts[1], 32)
			switch parts[0] {
			case "frame":
				progress.Frame = atoi
			case "fps":
				progress.FPS = float32(atof)
			case "bitrate":
				progress.BitRate = parts[1]
			case "total_size":
				progress.TotalSize = atoi
			case "out_time_us":
				progress.Duration, _ = time.ParseDuration(parts[1] + "us")
			case "dup_frames":
				progress.DupFrames = atoi
			case "drop_frames":
				progress.DroppedFrames = atoi
			case "speed":
				progress.Speed = parts[1]
			// NOTE: progress is always the last line, so yield here.
			case "progress":
				progress.Progress = FfmpegProgressState(parts[1])
				ch <- progress
			}
		}
		close(ch)
	}()
	return ch
}
