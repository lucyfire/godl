package cmdutil

import (
	"context"
	"sync"
)

// Context is a wrapper around context.Context
type Context struct {
	Ctx    context.Context
	Cancel context.CancelFunc
	// WaitGroup is used to wait for all goroutines to finish/cancel
	sync.WaitGroup
}

func newContext() *Context {
	ctx, cancel := context.WithCancel(context.Background())
	return &Context{
		Ctx:    ctx,
		Cancel: cancel,
	}
}
