package cmdutil

import (
	"fmt"
	"os"
	"path/filepath"
	"time"

	tea "github.com/charmbracelet/bubbletea"
	"gitlab.com/lucyfire/lucytea/confirm"
	"gitlab.com/lucyfire/lucytea/navstack"
)

func TeaFileExistsPrompt(outputFile string, followupCmd tea.Cmd) tea.Cmd {
	// file already exists
	if _, err := os.Stat(outputFile); err == nil {
		confirmCmd := tea.Sequence(func() tea.Msg {
			if err := os.Remove(outputFile); err != nil {
				return tea.Sequence(func() tea.Msg {
					return fmt.Errorf("failed to remove file with %s", err)
				}, func() tea.Msg {
					time.Sleep(time.Second * 2)
					return tea.QuitMsg{}
				})
			}
			return nil
		}, func() tea.Msg {
			return navstack.PopNavigable{}
		}, followupCmd)

		confirmModel := confirm.New(confirmCmd, tea.Quit)
		confirmModel.Message = fmt.Sprintf("File '%s' already exists. Overwrite?", filepath.Base(outputFile))
		return func() tea.Msg {
			return navstack.PushNavigable(confirmModel)
		}
	}
	return followupCmd
}
