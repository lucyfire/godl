package cmdutil

import (
	"errors"

	"github.com/spf13/cobra"
)

const (
	AnnotationFlagDocker         = "flag-docker"
	AnnotationFlagInteractive    = "flag-interactive"
	AnnotationFlagFileConstrains = "flag-file-validations"
	AnnotationFlagFileOutput     = "flag-file-output"

	FlagDocker         = "docker"
	FlagDaemon         = "daemon"
	FlagInteractive    = "interactive"
	FlagFileConstrains = "ignore-file-validations"
	FlagValidations    = "ignore-validations"
	FlagFileOutput     = "output"

	// MultiCommand commands cannot run in daemon on host machine
	// MultiCommand = "multi-command"

	// Path annotations should check against the given cmd exists in $PATH, else
	// throw an error.
	PathFfmpeg  = "ffmpeg"
	PathFfprobe = "ffprobe"
	PathYTDLP   = "yt-dlp"
)

var (
	ErrorMultiCommandDaemon       = errors.New("cannot run command as daemon on host machine, you can use --docker with --daemon")
	ErrorInteractiveCommandDaemon = errors.New("cannot run interactive command as daemon")
)

// revive:disable:cognitive-complexity
func (f *Factory) AddAnnotationFlags(cmd *cobra.Command) {
	for _, c := range cmd.Commands() {
		if c.Annotations != nil {
			if v, ok := c.Annotations[AnnotationFlagDocker]; ok && v == "true" {
				c.Flags().BoolVar(&f.ExecuteInDocker, FlagDocker, true, "Run command in docker")
			}
			if v, ok := c.Annotations[AnnotationFlagInteractive]; ok && v == "true" {
				c.Flags().BoolVar(&f.Interactive, FlagInteractive, false, "Interactive way to run the command, usually spawns a tui")
			}
			if v, ok := c.Annotations[AnnotationFlagFileConstrains]; ok && v == "true" {
				c.Flags().BoolVar(&f.IgnoreFileConstrains, FlagFileConstrains, false, "Ignore any file type validations")
			}
			if v, ok := c.Annotations[AnnotationFlagFileOutput]; ok && v == "true" {
				c.Flags().StringVarP(&f.Output, FlagFileOutput, "o", "", "The output filename")
			}
		}
		// Recursively apply to all subcommands
		f.AddAnnotationFlags(c)
	}
}

// revive:enable:cognitive-complexity

func (f *Factory) ValidateAnnotations(cmd *cobra.Command) error {
	if f.InsideDocker || cmd.Annotations == nil {
		return nil
	}
	if v, ok := cmd.Annotations[AnnotationFlagInteractive]; ok && v == "true" {
		if f.Daemon && cmd.Flag(FlagInteractive).Value.String() == "true" {
			return ErrorInteractiveCommandDaemon
		}
	}
	if f.IgnoreFileConstrains {
		return nil
	}
	return nil
}
