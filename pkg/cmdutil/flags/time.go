package flags

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type Time struct {
	Hours        int
	Minutes      int
	Seconds      int
	Milliseconds int
	Duration     time.Duration
}

func (t *Time) Set(s string) error {
	if err := t.Validate(s); err != nil {
		return err
	}
	parts := strings.Split(s, ":")
	t.Hours, _ = strconv.Atoi(parts[0])
	t.Minutes, _ = strconv.Atoi(parts[1])
	secondParts := strings.Split(parts[2], ".")
	t.Seconds, _ = strconv.Atoi(secondParts[0])
	if len(secondParts) > 1 {
		secondParts[1] += strings.Repeat("0", 3-len(secondParts[1]))
		t.Milliseconds, _ = strconv.Atoi(secondParts[1])
	}
	t.Duration = time.Duration(t.Hours)*time.Hour + time.Duration(t.Minutes)*time.Minute + time.Duration(t.Seconds)*time.Second + time.Duration(t.Milliseconds)*time.Millisecond
	return nil
}

func (*Time) Validate(s string) error {
	match, err := regexp.MatchString(`^(\d){2}:(\d){2}:(\d){2}(\.\d{1,3})?$`, s)
	if err != nil || !match {
		return errors.New("must be in the format {HH:MM:SS[.MS]}")
	}
	return nil
}

func (*Time) Type() string {
	return "Time"
}

func (t *Time) String() string {
	return fmt.Sprintf("%02d:%02d:%02d.%03d", t.Hours, t.Minutes, t.Seconds, t.Milliseconds)
}

func (t *Time) UnmarshalJSON(b []byte) (err error) {
	return t.Set(strings.Trim(string(b), "\""))
}

func (t *Time) MarshalJSON() ([]byte, error) {
	if t.Duration == 0 {
		return []byte{}, nil
	}
	return []byte(t.String()), nil
}
