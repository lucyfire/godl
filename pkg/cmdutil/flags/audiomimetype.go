package flags

import (
	"errors"
	"strings"

	"gitlab.com/lucyfire/godl/pkg/types"
)

type AudioMimeType types.Type

func (c *AudioMimeType) String() string {
	return c.Name
}

func (c *AudioMimeType) Set(s string) error {
	v, err := types.AudioMimeTypes().MatchByName(s)
	if err != nil {
		return errors.New(`must be one of "` + strings.Join(c.Values(), `" "`) + `"`)
	}
	*c = AudioMimeType(v)
	return nil
}

func (*AudioMimeType) Type() string {
	return "AudioMimeType"
}

func (*AudioMimeType) Values() []string {
	return types.AudioMimeTypes().Strings()
}
