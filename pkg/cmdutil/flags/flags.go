package flags

// HasSpecificValues flags must be one of the values returned from Values func
type HasSpecificValues interface {
	Values() []string
}
