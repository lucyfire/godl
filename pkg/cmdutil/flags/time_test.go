package flags_test

import (
	"fmt"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmdutil/flags"
)

var _ = Describe("Time", func() {
	var t *flags.Time
	BeforeEach(func() {
		t = &flags.Time{}
	})

	It("should correctly parse time string", func() {
		err := t.Set("01:02:03.5")
		Expect(err).To(BeNil())
		Expect(t.Hours).To(Equal(1))
		Expect(t.Minutes).To(Equal(2))
		Expect(t.Seconds).To(Equal(3))
		Expect(t.Milliseconds).To(Equal(500))
		Expect(t.Duration).To(Equal(1*time.Hour + 2*time.Minute + 3*time.Second + 500*time.Millisecond))
	})

	It("should fail with incorrect time string", func() {
		tests := []string{
			"001:02:03.5",
			"01:002:03.5",
			"01:02:003.5",
			"01:02:03.5000",
		}
		for _, test := range tests {
			err := t.Set(test)
			Expect(err).To(Equal(fmt.Errorf("must be in the format {HH:MM:SS[.MS]}")))
		}
	})
})
