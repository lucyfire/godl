package flags

import (
	"errors"
	"strings"

	"gitlab.com/lucyfire/godl/pkg/types"
)

type VideoCodec types.Type

func (c *VideoCodec) String() string {
	return c.Name
}

func (c *VideoCodec) Set(s string) error {
	v, err := types.VideoCodecs().MatchByName(s)
	if err != nil {
		return errors.New(`must be one of "` + strings.Join(c.Values(), `" "`) + `"`)
	}
	*c = VideoCodec(v)
	return nil
}

func (*VideoCodec) Type() string {
	return "VideoCodec"
}

func (*VideoCodec) Values() []string {
	return types.VideoCodecs().Strings()
}
