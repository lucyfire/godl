package cmdutil

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

var (
	DockerImage  string
	DockerLabels = map[string]string{"creator": "godl"}
)

// CreateFolder Checks if folder exists and if it doesn't it creates it.
func CreateFolder(path string) error {
	if _, err := os.Stat(path); err != nil {
		if err = os.Mkdir(path, os.ModePerm); err != nil {
			return fmt.Errorf("can't create folder: %s", path)
		}
	}
	return nil
}

func ReadFileIntoJSON(path string, jsonStruct any) error {
	metadataFile, err := os.Open(path)
	if err != nil {
		return errors.New("cannot open metadata file")
	}
	defer metadataFile.Close()
	metadataBytes, err := io.ReadAll(metadataFile)
	if err != nil {
		return errors.New("cannot read metadata file")
	}
	return json.Unmarshal(metadataBytes, &jsonStruct)
}

// PathsToMounts adds active directory, prepares dirs for docker volume binding
func PathsToMounts(paths ...string) (mounts map[string]string) {
	mounts = make(map[string]string)
	dirPath, _ := os.Getwd()
	dirPath = strings.TrimSuffix(dirPath, string(filepath.Separator))
	mounts[dirPath] = dirPath
	for _, d := range paths {
		if d == "" {
			continue
		}
		d = strings.TrimSuffix(filepath.Dir(d), string(filepath.Separator))
		mounts[d] = d
	}
	return mounts
}

// CanonicalizePaths updates each path to it's full system path.
// Deprecated: use InputFile
func CanonicalizePaths(paths ...*string) error {
	for _, path := range paths {
		if *path == "" {
			continue
		}
		p, err := filepath.Abs(*path)
		if err != nil {
			return err
		}
		*path = p
	}
	return nil
}

// DockerfyCommand returns a slice with all parts of the invoked command
func DockerfyCommand(cmd *cobra.Command, args []string) []string {
	invokedCmd := strings.Split(cmd.CommandPath(), " ")
	invokedCmd = append(invokedCmd, args...)

	// Visit all flags and collect their names and values
	cmd.Flags().VisitAll(func(flag *pflag.Flag) {
		if flag.Changed {
			switch flag.Name {
			case
				// We never want to rerun the command in docker and validations
				// have already been run on host so we skip them.
				FlagDocker, FlagFileConstrains, FlagValidations,
				// Always run the command normally inside docker.
				FlagDaemon:
				// Do nothing
			default:
				invokedCmd = append(invokedCmd, fmt.Sprintf("--%s=%s", flag.Name, flag.Value))
			}
		}
	})

	invokedCmd = append(invokedCmd, fmt.Sprintf("--%s=%s", FlagDocker, "false"))
	invokedCmd = append(invokedCmd, fmt.Sprintf("--%s=%s", FlagValidations, "true"))
	return invokedCmd
}
