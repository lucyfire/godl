package cmdutil

import (
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/lucyfire/docker"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type Factory struct {
	Logger *zap.Logger
	Atom   zap.AtomicLevel
	// Docker wrapper a helper struct to start containers
	// Deprecated: will be removed in v5.0.0
	Docker docker.Wrapper

	// Context is the context to be used for the command
	Context *Context
	// Output sets the output filename, applicable for commands with AnnotationFlagFileOutput annotation.
	Output string
	// Interactive starts a tui, applicable for commands with AnnotationFlagInteractive annotation.
	Interactive bool
	// ExecuteInDocker run the command inside docker
	ExecuteInDocker bool
	// IgnoreValidations run the command without performing any validations
	IgnoreValidations bool
	// IgnoreFileConstrains run the command without performing any InputFile validations
	IgnoreFileConstrains bool
	// Daemon run the command in daemon.
	// WARN: This can cause an orphaned process if the command expects any user input.
	Daemon bool
	// Debug set the Logger level to debug mode
	Debug bool
	// InsideDocker states if the command is currently running inside docker.
	InsideDocker bool
}

func NewFactory() *Factory {
	atom := zap.NewAtomicLevel()
	cfg := zap.NewProductionEncoderConfig()
	cfg.TimeKey = "timestamp"
	cfg.EncodeTime = zapcore.ISO8601TimeEncoder
	cfg.EncodeLevel = zapcore.CapitalColorLevelEncoder
	logger := zap.New(zapcore.NewCore(zapcore.NewConsoleEncoder(cfg), zapcore.Lock(os.Stdout), atom))

	return &Factory{
		Logger:       logger,
		Atom:         atom,
		Context:      newContext(),
		InsideDocker: os.Getenv("GODL_RUNNING_IN_DOCKER") == "true",
	}
}

func (f *Factory) CleanupHandler() {
	signalCh := make(chan os.Signal, 1)
	go func() {
		s := <-signalCh
		f.Logger.Info("Caught Signal", zap.String("signal", s.String()))
		f.Context.Cancel()
	}()
	signal.Notify(signalCh, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
}
