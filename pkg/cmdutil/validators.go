package cmdutil

import (
	"errors"
	"fmt"
	"os"

	"github.com/h2non/filetype"
)

func IsFileAudio(filepath string) (bool, error) {
	buf, err := os.ReadFile(filepath)
	if err != nil {
		return false, fmt.Errorf("cannot read file '%s'", filepath)
	}
	return filetype.IsAudio(buf), nil
}

func IsFileVideo(filepath string) (bool, error) {
	buf, err := os.ReadFile(filepath)
	if err != nil {
		return false, errors.New("cannot read file")
	}
	return filetype.IsVideo(buf), nil
}

func IsFileImage(filepath string) (bool, error) {
	buf, err := os.ReadFile(filepath)
	if err != nil {
		return false, errors.New("cannot read file")
	}
	return filetype.IsImage(buf), nil
}

func IsFileAudioOrVideo(filepath string) (isAudio, isVideo bool, err error) {
	buf, err := os.ReadFile(filepath)
	if err != nil {
		return false, false, errors.New("cannot read file")
	}
	return filetype.IsAudio(buf), filetype.IsVideo(buf), nil
}

func BindInputFile(f *InputFile, path string) error {
	f.Path = path
	return f.Canonicalize()
}
