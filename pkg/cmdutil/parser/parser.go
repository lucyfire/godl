package parser

import (
	"io"

	tea "github.com/charmbracelet/bubbletea"
	"gitlab.com/lucyfire/lucytea/progress"
)

type TeaProgress interface {
	Progress() int
}

type TeaProgressParser interface {
	Parse() <-chan TeaProgress
}

func HandleTeaProgress(parser TeaProgressParser, progressModel *progress.Model, r io.ReadCloser, followupCmd tea.Cmd) (tea.Cmd, <-chan TeaProgress) {
	ch := make(chan TeaProgress)
	return tea.Sequence(func() tea.Msg {
		go func() {
			for progress := range parser.Parse() {
				progressModel.Current = progress.Progress()
				ch <- progress
			}
			progressModel.Current = progressModel.Total
		}()
		return struct{}{}
	}, progressModel.Init(), followupCmd), ch
}
