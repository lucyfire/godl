package parser

import (
	"bufio"
	"io"
	"strconv"
	"strings"
	"time"
)

type FfmpegProgressState string

const (
	FfmpegProgressContinue FfmpegProgressState = "continue"
	FfmpegProgressEnd      FfmpegProgressState = "end"
)

type FfmpegProgress struct {
	StartTime     time.Time
	BitRate       string
	Speed         string
	progress      FfmpegProgressState
	Duration      time.Duration
	Frame         int
	TotalSize     int
	DupFrames     int
	DroppedFrames int
	FPS           float32
}

func (p FfmpegProgress) Progress() int {
	return int(p.Duration.Microseconds())
}

type FfmpegProgressParser struct {
	reader io.Reader
}

func NewFfmpegParser(r io.Reader) TeaProgressParser {
	return &FfmpegProgressParser{r}
}

// Parse the ffmpeg progress output.
func (p *FfmpegProgressParser) Parse() <-chan TeaProgress {
	ch := make(chan TeaProgress)
	go func() {
		progress := new(FfmpegProgress)
		progress.StartTime = time.Now()
		scanner := bufio.NewScanner(p.reader)
		for scanner.Scan() {
			t := scanner.Text()
			parts := strings.SplitN(t, "=", 2)
			if len(parts) != 2 {
				continue
			}
			atoi, _ := strconv.Atoi(parts[1])
			atof, _ := strconv.ParseFloat(parts[1], 32)
			switch parts[0] {
			case "frame":
				progress.Frame = atoi
			case "fps":
				progress.FPS = float32(atof)
			case "bitrate":
				progress.BitRate = parts[1]
			case "total_size":
				progress.TotalSize = atoi
			case "out_time_us":
				progress.Duration, _ = time.ParseDuration(parts[1] + "us")
			case "dup_frames":
				progress.DupFrames = atoi
			case "drop_frames":
				progress.DroppedFrames = atoi
			case "speed":
				progress.Speed = parts[1]
			// NOTE: progress is always the last line, so yield here.
			case "progress":
				progress.progress = FfmpegProgressState(parts[1])
				ch <- progress
			}
		}
		close(ch)
	}()
	return ch
}
