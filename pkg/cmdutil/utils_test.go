package cmdutil_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

var _ = Describe("Utilities", func() {
	It("should correctly canonicalize paths", func() {
		path1 := "/path/to/some/file.md"
		path2 := "/path/to/some/../some/file.md"
		path3 := "/path/to/some/../../to/some/file.md"
		err := cmdutil.CanonicalizePaths(&path1, &path2, &path3)
		Expect(err).To(BeNil())
		Expect(path1).To(Equal("/path/to/some/file.md"))
		Expect(path2).To(Equal("/path/to/some/file.md"))
		Expect(path3).To(Equal("/path/to/some/file.md"))
	})
})
