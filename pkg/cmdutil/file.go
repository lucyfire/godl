package cmdutil

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"github.com/h2non/filetype"
	"github.com/pkg/errors"
)

var (
	ErrorNotVideoFile     = errors.New("file provided is not in a video format, can be suppressed with --ignore-file-validations flag")
	ErrorNotImageFile     = errors.New("file provided is not in an image format, can be suppressed with --ignore-file-validations flag")
	ErrorNotAudioFile     = errors.New("file provided is not in an audio format, can be suppressed with --ignore-file-validations flag")
	ErrorNotAcceptedFile  = errors.New("file provided is not in an acceptable format, can be suppressed with --ignore-file-validations flag")
	ErrorFileDoesNotExist = errors.New("file does not exist")
)

// InputFileConstrains are constrains run against the file
type InputFileConstrains func(f *InputFile) error

func VideoFile(f *InputFile) error {
	if !f.IsVideo() {
		return ErrorNotVideoFile
	}
	return nil
}

func AudioFile(f *InputFile) error {
	if !f.IsAudio() {
		return ErrorNotAudioFile
	}
	return nil
}

func ImageFile(f *InputFile) error {
	if !f.IsImage() {
		return ErrorNotImageFile
	}
	return nil
}

func fileExists(f *InputFile) error {
	if _, err := os.Stat(f.Path); os.IsNotExist(err) {
		return ErrorFileDoesNotExist
	}
	return nil
}

type InputFile struct {
	// Name of the file including Ext
	Name string
	// BaseName name of the file without Ext
	BaseName string
	// Ext of the file including "."
	Ext string
	// Path to the file
	Path        string
	readErr     error
	constraints []InputFileConstrains
	buf         []byte
	// read the file once and store the error and buffer
	readOnce          sync.Once
	IgnoreConstraints bool
}

func NewInputFile(constraints ...InputFileConstrains) *InputFile {
	return &InputFile{
		constraints: constraints,
	}
}

// ValidateAnd checks the file against all constrains, if any of them fail, return an error
func (f *InputFile) ValidateAnd() error {
	if fileExists(f) != nil {
		return ErrorFileDoesNotExist
	}
	for _, constraint := range f.constraints {
		if err := constraint(f); err != nil {
			return err
		}
	}
	return nil
}

// ValidateOr checks the file until one constrain passes, if none do, return an error
func (f *InputFile) ValidateOr() error {
	if fileExists(f) != nil {
		return ErrorFileDoesNotExist
	}
	var matchedOnce bool
	for _, constraint := range f.constraints {
		if err := constraint(f); err == nil {
			matchedOnce = true
			break
		}
	}
	if matchedOnce {
		return nil
	}
	return ErrorNotAcceptedFile
}

// Canonicalize gets the absolute system path of the current InputFile.
func (f *InputFile) Canonicalize() error {
	p, err := filepath.Abs(f.Path)
	if err != nil {
		return fmt.Errorf("failed to canonicalize file %s", f.Path)
	}
	f.Path = p
	f.Name = filepath.Base(f.Path)
	f.Ext = filepath.Ext(f.Path)
	f.BaseName = strings.TrimSuffix(f.Name, f.Ext)

	return nil
}

func (f *InputFile) ReadFile() ([]byte, error) {
	f.readOnce.Do(func() {
		buf, err := os.ReadFile(f.Path)
		if err != nil {
			f.readErr = fmt.Errorf("cannot read file %s", f.Path)
			return
		}
		f.buf = buf
	})

	return f.buf, f.readErr
}

// IsImage assumes the file has already been read into memory.
func (f *InputFile) IsImage() bool {
	buf, err := f.ReadFile()
	if err != nil {
		return false
	}
	return filetype.IsImage(buf)
}

// IsVideo assumes the file has already been read into memory.
func (f *InputFile) IsVideo() bool {
	buf, err := f.ReadFile()
	if err != nil {
		return false
	}
	return filetype.IsVideo(buf)
}

// IsAudio assumes the file has already been read into memory.
func (f *InputFile) IsAudio() bool {
	buf, err := f.ReadFile()
	if err != nil {
		return false
	}
	return filetype.IsAudio(buf)
}

// Set satisfy the flag interface
func (f *InputFile) Set(path string) error {
	f.Path = path
	return f.Canonicalize()
}

// Type satisfy the flag interface
func (*InputFile) Type() string {
	return "Input File"
}

// String satisfy the flag interface
func (f *InputFile) String() string {
	return f.Path
}
