package cmdutiltest

import (
	"errors"
	"os"
	"path/filepath"
	"strings"
)

func RootDir() (string, error) {
	wd, _ := os.Getwd()
	for !strings.HasSuffix(strings.ToLower(wd), "godl") {
		wd = filepath.Dir(wd)

		if wd == string(os.PathSeparator) {
			return "", errors.New("root directory not found")
		}
	}
	return wd, nil
}
