package cmdutiltest

import (
	"path/filepath"
)

const (
	AudioFileMp3      = "audio-file.mp3"
	VideoFileMp4      = "video-file.mp4"
	MetadataFileJSON  = "chop-medata.json"
	ThumbnailFileJPEG = "thumbnail.jpeg"
)

func FilePath(file string) (string, error) {
	rd, err := RootDir()
	if err != nil {
		return "", err
	}
	return filepath.Join(rd, "test", file), nil
}
