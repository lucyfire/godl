package runner

import (
	"context"
	"io"
	"os"
	"os/exec"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/lucyfire/docker/v2"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"golang.org/x/term"
)

type CommandV2 interface {
	// Prepare is called before the command is executed
	Prepare() error
	Command() []string
	ContainerConfig() docker.ContainerConfig
}

func Execute(f *cmdutil.Factory, c CommandV2, out, err io.Writer, in io.Reader) error {
	f.Context.Add(1)
	defer f.Context.Done()
	if err := c.Prepare(); err != nil {
		return err
	}
	parts := c.Command()
	cmd := exec.CommandContext(f.Context.Ctx, parts[0], parts[1:]...)
	cmd.Stdout = out
	cmd.Stderr = err
	cmd.Stdin = in
	return cmd.Run()
}

func ExecuteDaemon(c CommandV2) error {
	if err := c.Prepare(); err != nil {
		return err
	}
	parts := c.Command()
	cmd := exec.Command(parts[0], parts[1:]...)
	return cmd.Start()
}

func StartDocker(ctx context.Context, f *cmdutil.Factory, c CommandV2) error {
	cfg := c.ContainerConfig()
	container, err := docker.NewContainer(cfg)
	if err != nil {
		return err
	}
	if err := container.Create(ctx); err != nil {
		return err
	}
	var ios docker.IOStreamer
	if !f.Daemon {
		ios, err = container.AttachIO(ctx)
		if err != nil {
			return err
		}
	}

	err = container.Start(ctx, 5*time.Second)
	if err != nil {
		return err
	}

	if f.Daemon {
		return nil
	}

	// Set terminal to raw mode to handle interactive input
	oldState, _ := term.MakeRaw(int(os.Stdin.Fd()))
	defer term.Restore(int(os.Stdin.Fd()), oldState)
	width, height, _ := term.GetSize(int(os.Stdout.Fd()))
	container.Resize(ctx, uint(width), uint(height))
	// Listen for resize events
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGWINCH)
	go func() {
		for range sigs {
			width, height, _ := term.GetSize(int(os.Stdout.Fd()))
			container.Resize(ctx, uint(width), uint(height))
		}
	}()

	ios.Start()
	if !cfg.AutoRemove {
		defer container.Stop(context.Background())
	}
	ios.Wait(ctx)
	return container.Wait(ctx)
}
