package runner

import (
	"context"
	"os"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/docker"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"go.uber.org/zap"
)

type Command interface {
	DockerCommand() docker.Command
	ContainerConfig() docker.ContainerConfig
}

type CommandWithExecs interface {
	Command
	Execs(f *cmdutil.Factory, container docker.Container) []error
}

func RunCommand(cmd *cobra.Command, f *cmdutil.Factory, realCmd Command) {
	f.CleanupHandler()

	cfg := realCmd.ContainerConfig()
	if cfg.WorkingDir == "" {
		cfg.WorkingDir, _ = os.Getwd()
	}
	if !f.Daemon {
		cfg.AutoRemove = false
	}
	container, err := f.Docker.ContainerCreate(f.Context.Ctx, cfg)
	if err != nil {
		f.Logger.Error(cmd.Name(), zap.String("operation", "Create container"), zap.Error(err))
		return
	}

	var ios docker.IOStreamer

	if !f.Daemon {
		defer container.Remove(f.Context.Ctx, f.Docker)
		defer container.Stop(context.Background(), f.Docker)
		if ios, err = container.AttachIO(f.Context.Ctx, f.Docker); err != nil {
			f.Logger.Error(cmd.Name(), zap.String("operation", "Attach container IO"), zap.Error(err))
		}
	}
	if err = container.Start(f.Context.Ctx, f.Docker, 5*time.Second); err != nil {
		f.Logger.Error(cmd.Name(), zap.String("operation", "Container start"), zap.Error(err))
	}
	if !f.Daemon {
		if err := ios.Wait(f.Context.Ctx); err != nil {
			f.Logger.Error(cmd.Name(), zap.String("operation", "IO wait"), zap.Error(err))
			return
		}
		if err = container.Wait(f.Context.Ctx, f.Docker); err != nil {
			f.Logger.Error(cmd.Name(), zap.String("operation", "Container wait"), zap.Error(err))
		}
	}
}

func RunCommandWithStartedContainer(cmd *cobra.Command, f *cmdutil.Factory, realCmd CommandWithExecs) {
	f.CleanupHandler()

	cfg := realCmd.ContainerConfig()
	if cfg.WorkingDir == "" {
		cfg.WorkingDir, _ = os.Getwd()
	}
	container, err := f.Docker.ContainerStart(f.Context.Ctx, cfg)
	if err != nil {
		f.Logger.Error(cmd.Name(), zap.Error(err))
		return
	}
	defer container.Stop(context.Background(), f.Docker)

	errors := realCmd.Execs(f, container)
	if errors != nil {
		f.Logger.Error(cmd.Name(), zap.Errors("errors", errors))
	}
}
