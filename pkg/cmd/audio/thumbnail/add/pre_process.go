package add

import (
	"errors"

	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

func (c *Command) Validate() error {
	if c.IgnoreFileValidations {
		return nil
	}
	if isAudio, err := cmdutil.IsFileAudio(c.FilePath); err != nil || !isAudio {
		return errors.New("audio file provided isn't in an audio format")
	}
	if isImage, err := cmdutil.IsFileImage(c.ThumbnailPath); err != nil || !isImage {
		return errors.New("thumbnail provided isn't in an image format")
	}
	return nil
}
