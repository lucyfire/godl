package add

import (
	"errors"

	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/runner"
)

func NewCmdAdd(f *cmdutil.Factory) *cobra.Command {
	realCmd := &Command{}
	cmd := &cobra.Command{
		Use:     "add { <audio-file> }",
		Short:   "Adds thumbnail to an audio file",
		Example: "$ godl audio thumbnail add audiofile.mp3 --thumbnail thumbnail.jpg [ --ignore-file-validations ]",
		Args: func(cmd *cobra.Command, args []string) error {
			if err := cobra.ExactArgs(1)(cmd, args); err != nil {
				return err
			}
			realCmd.FilePath = args[0]
			if err := cmdutil.CanonicalizePaths(&realCmd.FilePath, &realCmd.ThumbnailPath); err != nil {
				return errors.New("failed to canonicalize file paths")
			}
			if f.IgnoreValidations {
				return nil
			}
			return realCmd.Validate()
		},
		Run: func(cmd *cobra.Command, _ []string) {
			runner.RunCommand(cmd, f, realCmd)
		},
	}

	cmd.Flags().StringVarP(&realCmd.ThumbnailPath, "thumbnail", "t", "", "The thumbnail")
	cmd.Flags().BoolVar(&realCmd.IgnoreFileValidations, "ignore-file-validations", false, "Ignore any file type validations")
	cmd.MarkFlagRequired("thumbnail")
	return cmd
}
