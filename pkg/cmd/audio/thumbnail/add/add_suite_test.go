package add_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestAddCommand(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Add Command Suite")
}
