package add_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmd/audio/thumbnail/add"
)

var _ = Describe("Add Command", func() {
	command := add.Command{
		FilePath:      "some-file.mp3",
		ThumbnailPath: "some-image.png",
	}

	It("should correctly generate cli command", func() {
		Expect(command.DockerCommand().String()).To(Equal("eyeD3 --add-image some-image.png:FRONT_COVER:Album Cover some-file.mp3"))
	})
})
