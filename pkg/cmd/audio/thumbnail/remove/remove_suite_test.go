package remove_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestRemoveCommand(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Remove Command Suite")
}
