package remove_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmd/audio/thumbnail/remove"
)

var _ = Describe("Remove Command", func() {
	command := remove.Command{
		FilePath: "some-file.mp3",
	}

	It("should correctly generate cli command", func() {
		Expect(command.DockerCommand().String()).To(Equal("eyeD3 --remove-all-images some-file.mp3"))
	})
})
