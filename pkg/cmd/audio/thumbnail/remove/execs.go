package remove

import (
	"context"

	"gitlab.com/lucyfire/docker"
	"gitlab.com/lucyfire/godl/pkg/cmd/audio/thumbnail/extract"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

func (c *Command) Execs(f *cmdutil.Factory, container docker.Container) []error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	if c.WriteImages {
		if errs := c.execWriteImages(ctx, f, container); errs != nil {
			return errs
		}
	}
	exec, err := container.Exec(ctx, f.Docker, c.DockerCommand())
	if err != nil {
		return []error{err}
	}
	err = exec.Wait(ctx, f.Docker)
	if err != nil {
		return []error{err}
	}
	return nil
}

func (c *Command) execWriteImages(ctx context.Context, f *cmdutil.Factory, container docker.Container) []error {
	extractCmd := &extract.Command{FilePath: c.FilePath}
	e, err := container.Exec(ctx, f.Docker, extractCmd.DockerCommand())
	if err != nil {
		return []error{err}
	}
	if f.Daemon {
		if err := e.Start(ctx, f.Docker); err != nil {
			return []error{err}
		}
		if err = e.Wait(ctx, f.Docker); err != nil {
			return []error{err}
		}
	} else {
		ios, err := e.AttachIO(ctx, f.Docker)
		if err != nil {
			return []error{err}
		}
		if err = ios.Wait(ctx); err != nil {
			return []error{err}
		}
	}
	return nil
}
