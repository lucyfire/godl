package extract

import (
	"errors"

	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/runner"
)

func NewCmdExtract(f *cmdutil.Factory) *cobra.Command {
	realCmd := &Command{}
	cmd := &cobra.Command{
		Use:     "extract { <audio-filepath> }",
		Short:   "Extracts all images from an audio file to current directory",
		Example: "$ godl audio thumbnail extract audiofile.mp3 [ --ignore-file-validations ]",
		Args: func(cmd *cobra.Command, args []string) error {
			if err := cobra.ExactArgs(1)(cmd, args); err != nil {
				return err
			}
			realCmd.FilePath = args[0]
			if err := cmdutil.CanonicalizePaths(&realCmd.FilePath); err != nil {
				return errors.New("failed to canonicalize file paths")
			}
			if f.IgnoreValidations {
				return nil
			}
			return realCmd.Validate()
		},
		Run: func(cmd *cobra.Command, _ []string) {
			runner.RunCommand(cmd, f, realCmd)
		},
	}
	cmd.Flags().BoolVar(&realCmd.IgnoreFileValidations, "ignore-file-validations", false, "Ignore any file type validations")
	return cmd
}
