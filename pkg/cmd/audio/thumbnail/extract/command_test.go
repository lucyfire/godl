package extract_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmd/audio/thumbnail/extract"
)

var _ = Describe("Extract Command", func() {
	command := extract.Command{
		FilePath: "some-file.mp3",
	}

	It("should correctly generate cli command", func() {
		Expect(command.DockerCommand().String()).To(Equal("eyeD3 --write-images . some-file.mp3"))
	})
})
