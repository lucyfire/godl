package extract

import (
	"gitlab.com/lucyfire/docker"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

type Command struct {
	FilePath              string
	IgnoreFileValidations bool
}

func (c *Command) ContainerConfig() docker.ContainerConfig {
	return docker.ContainerConfig{
		Image:       cmdutil.DockerImage,
		Labels:      map[string]string{"creator": "godl"},
		Directories: cmdutil.PathsToMounts(c.FilePath),
		Command:     c.DockerCommand(),
		AutoRemove:  true,
	}
}

func (c *Command) DockerCommand() docker.Command {
	return docker.Command{
		"eyeD3",
		"--write-images",
		".",
		c.FilePath,
	}
}
