package extract

import (
	"errors"

	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

func (c *Command) Validate() error {
	if c.IgnoreFileValidations {
		return nil
	}
	if isAudio, err := cmdutil.IsFileAudio(c.FilePath); err != nil || !isAudio {
		return errors.New("audio file provided isn't in an audio format")
	}
	return nil
}
