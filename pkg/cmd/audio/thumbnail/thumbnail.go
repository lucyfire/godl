package thumbnail

import (
	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/godl/pkg/cmd/audio/thumbnail/add"
	"gitlab.com/lucyfire/godl/pkg/cmd/audio/thumbnail/extract"
	"gitlab.com/lucyfire/godl/pkg/cmd/audio/thumbnail/remove"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

func NewCmdThumbnail(f *cmdutil.Factory) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "thumbnail <command>",
		Short: "Thumbnail operations on an audio file",
	}

	cmdutil.AddGroup(cmd, "Audio thumbnail commands",
		add.NewCmdAdd(f),
		extract.NewCmdExtract(f),
		remove.NewCmdRemove(f),
	)
	return cmd
}
