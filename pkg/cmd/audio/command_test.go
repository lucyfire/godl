package audio_test

import (
	"net/url"

	"gitlab.com/lucyfire/godl/pkg/cmd/audio"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("Audio Command", func() {
	var command audio.Command
	BeforeEach(func() {
		command = audio.Command{}
		command.URL, _ = url.ParseRequestURI("https://some-url.com")
	})

	It("should correctly generate cli command", func() {
		Expect(command.DockerCommand().String()).To(Equal("yt-dlp --extract-audio --embed-thumbnail --audio-format mp3 --output %(title)s.%(ext)s https://some-url.com"))
		command.Output = "output"
		Expect(command.DockerCommand().String()).To(Equal("yt-dlp --extract-audio --embed-thumbnail --audio-format mp3 --output output.%(ext)s https://some-url.com"))
		command.KeepThumbnail = true
		Expect(command.DockerCommand().String()).To(Equal("yt-dlp --extract-audio --embed-thumbnail --write-thumbnail --audio-format mp3 --output output.%(ext)s https://some-url.com"))
	})
})
