package chop

import (
	"errors"
	"fmt"
	"path/filepath"

	"github.com/MakeNowJust/heredoc"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

type AudioChopTrack struct {
	// TODO: should automatically be parsed in flag.Time
	From string `json:"from"`
	// TODO: should automatically be parsed in flag.Time
	To          string `json:"to"`
	Title       string `json:"title"`
	Filename    string `json:"fileName"`
	TrackNumber int    `json:"trackNumber"`
}

type AudioChopAlbum struct {
	Artist      string           `json:"artist"`
	Genre       string           `json:"genre"`
	Album       string           `json:"album"`
	Tracks      []AudioChopTrack `json:"tracks"`
	TotalTracks int              `json:"totalTracks"`
	Year        int              `json:"year"`
}

func (c *Command) createChopFolders() error {
	if err := cmdutil.CreateFolder(c.Metadata.Artist); err != nil {
		return err
	}
	return cmdutil.CreateFolder(fmt.Sprintf("%s%c%s", c.Metadata.Artist, filepath.Separator, c.Metadata.Album))
}

func (c *Command) Validate() error {
	if err := c.ValidateFiles(); err != nil {
		return err
	}
	if err := cmdutil.ReadFileIntoJSON(c.MetadataPath, &c.Metadata); err != nil {
		return errors.New(heredoc.Doc(`
			json not in expected format
			Example:
			` + exampleMetadata()))
	}
	if len(c.Metadata.Tracks) == 0 {
		return errors.New("no tracks in metadata file, if you want to add metadata to this file use `godl audio metadata` command instead")
	}
	return nil
}

func (c *Command) ValidateFiles() error {
	if c.IgnoreFileValidations {
		return nil
	}
	if isAudio, err := cmdutil.IsFileAudio(c.FilePath); err != nil || !isAudio {
		return errors.New("audio file provided isn't in an audio format")
	}
	if c.ThumbnailPath != "" {
		if isImage, err := cmdutil.IsFileImage(c.ThumbnailPath); err != nil || !isImage {
			return errors.New("thumbnail provided isn't in an image format")
		}
	}
	return nil
}

func exampleMetadata() string {
	return `{
			"artist": "Mephorash",
			"genre": "Black Metal",
			"album": "Rites of Nullification",
			"totalTracks": 4,
			"year": 2015,
			"tracks": [
				{
					"trackNumber": 1,
					"from": "00:00:00",
					"to": "00:09:28",
					"title": "Riphyon - The Tree of Assiyah Putrescent",
					"fileName": "Riphyon - The Tree of Assiyah Putrescent"
				},
				{
					"trackNumber": 2,
					"from": "00:09:29",
					"to": "00:20:11",
					"title": "Phezur - Dissolving the Sea of Yetzirah",
					"fileName": "Phezur - Dissolving the Sea of Yetzirah"
				},
				{
					"trackNumber": 3,
					"from": "00:20:12",
					"to": "00:30:53",
					"title": "Cheidolun - Breaking the Blade of Beriah",
					"fileName": "Cheidolun - Breaking the Blade of Beriah"
				},
				{
					"trackNumber": 4,
					"from": "00:30:54",
					"to": "00:42:39",
					"title": "Berberioth - Vandalising the Throne of Atziluth",
					"fileName": "Berberioth - Vandalising the Throne of Atziluth"
				}
			]
		}`
}
