package chop_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestChopCommand(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Chop Command Suite")
}
