package chop

import (
	"context"
	"fmt"
	"path/filepath"
	"sync"

	"gitlab.com/lucyfire/docker"
	"gitlab.com/lucyfire/godl/pkg/cmd/audio/metadata"
	"gitlab.com/lucyfire/godl/pkg/cmd/slice"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"go.uber.org/zap"
)

func (c *Command) Execs(f *cmdutil.Factory, container docker.Container) []error {
	f.Logger.Info("Slicing track into tracks")
	// Slice all tracks concurrently
	errors := c.SliceTrack(f, container)
	if errors != nil {
		return errors
	}

	f.Logger.Info("Adding metadata to tracks")
	// Add metadata to all tracks concurrently
	errors = c.AddMetadata(f, container)
	if errors != nil {
		return errors
	}

	return nil
}

func (c *Command) SliceTrack(f *cmdutil.Factory, container docker.Container) []error {
	var (
		wg     sync.WaitGroup
		errors []error
	)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	for _, t := range c.Metadata.Tracks {
		wg.Add(1)
		go func(track AudioChopTrack) {
			defer wg.Done()
			sliceCmd := slice.Command{
				Output:   fmt.Sprintf("%s%c%s%c%s", c.Metadata.Artist, filepath.Separator, c.Metadata.Album, filepath.Separator, track.Filename),
				FilePath: c.FilePath,
			}
			sliceCmd.From.Set(track.From)
			sliceCmd.To.Set(track.To)
			e, err := container.Exec(ctx, f.Docker, sliceCmd.DockerCommand())
			if err != nil {
				f.Logger.Error("slice create exec", zap.String("filename", track.Filename), zap.Error(err))
				errors = append(errors, err)
				return
			}
			if err = e.Start(ctx, f.Docker); err != nil {
				f.Logger.Error("slice start exec ", zap.String("filename", track.Filename), zap.Error(err))
				errors = append(errors, err)
				return
			}
			if err = e.Wait(ctx, f.Docker); err != nil {
				f.Logger.Error("slice wait exec", zap.String("filename", track.Filename), zap.Error(err))
				errors = append(errors, err)
				return
			}
		}(t)
	}
	wg.Wait()
	return errors
}

func (c *Command) AddMetadata(f *cmdutil.Factory, container docker.Container) []error {
	var (
		wg     sync.WaitGroup
		errors []error
	)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	for _, t := range c.Metadata.Tracks {
		wg.Add(1)
		go func(track AudioChopTrack) {
			defer wg.Done()
			metadataCmd := &metadata.Command{
				FilePath: fmt.Sprintf("%s%c%s%c%s%s",
					c.Metadata.Artist,
					filepath.Separator,
					c.Metadata.Album,
					filepath.Separator,
					track.Filename,
					filepath.Ext(c.FilePath),
				),
				ThumbnailPath:    c.ThumbnailPath,
				Artist:           c.Metadata.Artist,
				Title:            track.Title,
				Album:            c.Metadata.Album,
				Genre:            c.Metadata.Genre,
				Year:             c.Metadata.Year,
				TrackNumber:      track.TrackNumber,
				TrackTotalNumber: c.Metadata.TotalTracks,
			}
			e, err := container.Exec(ctx, f.Docker, metadataCmd.DockerCommand())
			if err != nil {
				f.Logger.Error("metadata create exec", zap.String("filename", track.Filename), zap.Error(err))
				errors = append(errors, err)
				return
			}
			if err = e.Start(ctx, f.Docker); err != nil {
				f.Logger.Error("metadata start exec ", zap.String("filename", track.Filename), zap.Error(err))
				errors = append(errors, err)
				return
			}
			if err = e.Wait(ctx, f.Docker); err != nil {
				f.Logger.Error("metadata wait exec", zap.String("filename", track.Filename), zap.Error(err))
				errors = append(errors, err)
				return
			}
		}(t)
	}
	wg.Wait()
	return errors
}
