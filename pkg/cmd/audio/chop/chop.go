package chop

import (
	"errors"

	"go.uber.org/zap"

	"github.com/MakeNowJust/heredoc"
	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/runner"
)

func NewCmdChop(f *cmdutil.Factory) *cobra.Command {
	realCmd := &Command{}
	cmd := &cobra.Command{
		Use:     "chop { <audio-file> }",
		Short:   "Chops an audio file into sub-files and appends metadata to it",
		Example: "$ godl audio chop full-album-file.mp3 --metadata metadata.json [ --thumbnail thumbnail.jpg ]",
		Long: heredoc.Doc(`
			Used to chop a big audio file into smaller files, used to chop a whole music
			album into its corresponding tracks. Can also optionally embed a thumbnail to
			all chopped files. 
		`),
		Args: func(cmd *cobra.Command, args []string) error {
			if f.Daemon {
				return errors.New("chop command doesn't support daemon run, yet")
			}
			if err := cobra.ExactArgs(1)(cmd, args); err != nil {
				return err
			}
			realCmd.FilePath = args[0]
			if err := cmdutil.CanonicalizePaths(&realCmd.FilePath, &realCmd.ThumbnailPath, &realCmd.MetadataPath); err != nil {
				return errors.New("failed to canonicalize file paths")
			}
			if f.IgnoreValidations {
				return nil
			}
			return realCmd.Validate()
		},
		Run: func(cmd *cobra.Command, _ []string) {
			if err := realCmd.createChopFolders(); err != nil {
				f.Logger.Error(cmd.Name(), zap.Error(err))
				return
			}
			runner.RunCommandWithStartedContainer(cmd, f, realCmd)
		},
	}

	cmd.Flags().StringVarP(&realCmd.ThumbnailPath, "thumbnail", "t", "", "The thumbnail to add to sub-files")
	cmd.Flags().StringVarP(&realCmd.MetadataPath, "metadata", "m", "", heredoc.Doc(`
		A json file containing metadata
		Example:
		`+exampleMetadata()))
	cmd.Flags().BoolVar(&realCmd.IgnoreFileValidations, "ignore-file-validations", false, "Ignore any file type validations")

	cmd.MarkFlagRequired("metadata")

	return cmd
}
