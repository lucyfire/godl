package chop_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmd/audio/chop"
	"gitlab.com/lucyfire/godl/pkg/cmdutiltest"
)

var _ = Describe("Chop Command", func() {
	var command chop.Command

	BeforeEach(func() {
		audio, err := cmdutiltest.FilePath(cmdutiltest.AudioFileMp3)
		Expect(err).To(BeNil())
		thumbnail, err := cmdutiltest.FilePath(cmdutiltest.ThumbnailFileJPEG)
		Expect(err).To(BeNil())
		metadata, err := cmdutiltest.FilePath(cmdutiltest.MetadataFileJSON)
		Expect(err).To(BeNil())

		command = chop.Command{
			FilePath:      audio,
			ThumbnailPath: thumbnail,
			MetadataPath:  metadata,
		}
	})

	It("should correctly generate cli command", func() {
		Expect(command).To(Not(BeNil()))
	})

})
