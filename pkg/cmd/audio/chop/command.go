package chop

import (
	"gitlab.com/lucyfire/docker"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

type Command struct {
	FilePath              string
	ThumbnailPath         string
	MetadataPath          string
	Metadata              AudioChopAlbum
	IgnoreFileValidations bool
}

func (c *Command) ContainerConfig() docker.ContainerConfig {
	return docker.ContainerConfig{
		Image:       cmdutil.DockerImage,
		Labels:      map[string]string{"creator": "godl"},
		Directories: cmdutil.PathsToMounts(c.FilePath, c.ThumbnailPath, c.MetadataPath),
		Command:     c.DockerCommand(),
		AutoRemove:  true,
	}
}

func (*Command) DockerCommand() docker.Command {
	return docker.Command{"sleep", "infinity"}
}
