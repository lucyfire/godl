package metadata

import (
	"errors"

	"github.com/MakeNowJust/heredoc"
	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/runner"
)

func NewCmdMetadata(f *cmdutil.Factory) *cobra.Command {
	realCmd := &Command{}
	cmd := &cobra.Command{
		Use:   "metadata { <audio-file> }",
		Short: "Adds metadata to an audio file",
		Long:  "Adds at least 1 metadata option to an audio file",
		Example: heredoc.Doc(`
			$ godl audio metadata audio-file.mp3 \
				--thumbnail thumbnail.jpg \
				--artist Mephorash \
				--title "Riphyon - The Tree of Assiyah Putrescent" \
				--album "Rites of Nullification" \
				--year 2015 \
				--genre "Black Metal" \
				--track-number 1 \
				--track-total 4 \
				--lyrics lyrics-file.lrc \
				[ --ignore-file-validations ]
			
		`),
		Args: func(cmd *cobra.Command, args []string) error {
			if err := cobra.ExactArgs(1)(cmd, args); err != nil {
				return err
			}
			realCmd.FilePath = args[0]
			if err := cmdutil.CanonicalizePaths(&realCmd.FilePath, &realCmd.ThumbnailPath, &realCmd.LyricsFile); err != nil {
				return errors.New("failed to canonicalize file paths")
			}
			if f.IgnoreValidations {
				return nil
			}
			return realCmd.Validate()
		},
		Run: func(cmd *cobra.Command, _ []string) {
			runner.RunCommand(cmd, f, realCmd)
		},
	}

	cmd.Flags().StringVarP(&realCmd.ThumbnailPath, "thumbnail", "t", "", "The thumbnail to add to sub-files")
	cmd.Flags().StringVar(&realCmd.Artist, "artist", "", "Artist name")
	cmd.Flags().StringVar(&realCmd.Title, "title", "", "Title name")
	cmd.Flags().StringVar(&realCmd.Album, "album", "", "Album's name")
	cmd.Flags().IntVar(&realCmd.Year, "year", 0, "Song's release year")
	cmd.Flags().StringVar(&realCmd.Genre, "genre", "", "Song's genre")
	cmd.Flags().IntVar(&realCmd.TrackNumber, "track-number", -1, "TrackNumber's number on the album (use 0 to clear)")
	cmd.Flags().IntVar(&realCmd.TrackTotalNumber, "track-total-number", -1, "Total number of tracks on the album (use 0 to clear)")
	cmd.Flags().StringVar(&realCmd.LyricsFile, "lyrics-file", "", "A lyrics file")
	cmd.Flags().BoolVar(&realCmd.IgnoreFileValidations, "ignore-file-validations", false, "Ignore any file type validations")
	cmd.MarkFlagsRequiredTogether("track-number", "track-total-number")
	return cmd
}
