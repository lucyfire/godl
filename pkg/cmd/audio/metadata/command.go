package metadata

import (
	"strconv"

	"gitlab.com/lucyfire/docker"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

type Command struct {
	FilePath              string
	ThumbnailPath         string
	Artist                string
	Title                 string
	Album                 string
	Genre                 string
	LyricsFile            string
	Year                  int
	TrackNumber           int
	TrackTotalNumber      int
	IgnoreFileValidations bool
}

func (c *Command) ContainerConfig() docker.ContainerConfig {
	return docker.ContainerConfig{
		Image:       cmdutil.DockerImage,
		Labels:      map[string]string{"creator": "godl"},
		Directories: cmdutil.PathsToMounts(c.FilePath, c.ThumbnailPath, c.LyricsFile),
		Command:     c.DockerCommand(),
		AutoRemove:  true,
	}
}

func (c *Command) DockerCommand() docker.Command {
	cmd := docker.Command{"eyeD3"}
	if c.ThumbnailPath != "" {
		cmd = append(cmd, []string{"--add-image", c.ThumbnailPath + ":FRONT_COVER:" + "Album Cover"}...)
	}
	if c.Artist != "" {
		cmd = append(cmd, []string{"--artist", c.Artist}...)
	}
	if c.Title != "" {
		cmd = append(cmd, []string{"--title", c.Title}...)
	}
	if c.Album != "" {
		cmd = append(cmd, []string{"--album", c.Album}...)
	}
	if c.Year != 0 {
		cmd = append(cmd, []string{"--release-year", strconv.Itoa(c.Year)}...)
	}
	if c.Genre != "" {
		cmd = append(cmd, []string{"--genre", c.Genre}...)
	}
	if c.LyricsFile != "" {
		cmd = append(cmd, []string{"--add-lyrics", c.LyricsFile}...)
	}
	if c.TrackNumber != -1 {
		cmd = append(cmd, []string{
			"--track", strconv.Itoa(c.TrackNumber),
			"--track-total", strconv.Itoa(c.TrackTotalNumber),
		}...)
	}
	return append(cmd, c.FilePath)
}
