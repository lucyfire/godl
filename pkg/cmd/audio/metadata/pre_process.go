package metadata

import (
	"errors"
	"os"

	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

func (c *Command) Validate() error {
	if err := c.validateFile(); err != nil {
		return err
	}
	if c.ThumbnailPath == "" && c.Artist == "" && c.Title == "" && c.Album == "" && c.Year == 0 && c.Genre == "" && c.TrackNumber == -1 && c.TrackTotalNumber == -1 && c.LyricsFile == "" {
		return errors.New("atleast one flag required")
	}
	if c.Year < 0 {
		return errors.New("year flag can't be negative number")
	}
	if c.TrackNumber != -1 && c.TrackNumber <= 0 {
		return errors.New("track number should be 0 or greater")
	}
	if c.TrackTotalNumber != -1 && c.TrackTotalNumber < c.TrackNumber {
		return errors.New("total number of tracks should be equal or greater than the provided track number")
	}
	return nil
}

func (c *Command) validateFile() error {
	if c.IgnoreFileValidations {
		return nil
	}
	if c.ThumbnailPath != "" {
		if isImage, err := cmdutil.IsFileImage(c.ThumbnailPath); err != nil || !isImage {
			return errors.New("thumbnail provided isn't in an image format")
		}
	}
	if c.LyricsFile != "" {
		if _, err := os.ReadFile(c.LyricsFile); err != nil {
			return errors.New("lyric file provided can't be opened")
		}
	}
	return nil
}
