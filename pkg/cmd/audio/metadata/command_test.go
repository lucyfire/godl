package metadata_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmd/audio/metadata"
)

var _ = Describe("Metadata Command", func() {
	var command metadata.Command
	BeforeEach(func() {
		command = metadata.Command{
			FilePath:         "some-file.mp3",
			TrackNumber:      -1,
			TrackTotalNumber: -1,
		}
	})

	It("should correctly generate cli command with thumbnail", func() {
		command.ThumbnailPath = "some-image.png"
		Expect(command.DockerCommand().String()).To(Equal("eyeD3 --add-image some-image.png:FRONT_COVER:Album Cover some-file.mp3"))
	})

	It("should correctly generate cli command with artist", func() {
		command.Artist = "Artist Name"
		Expect(command.DockerCommand().String()).To(Equal("eyeD3 --artist Artist Name some-file.mp3"))
	})

	It("should correctly generate cli command with title", func() {
		command.Title = "Song Name"
		Expect(command.DockerCommand().String()).To(Equal("eyeD3 --title Song Name some-file.mp3"))
	})

	It("should correctly generate cli command with album", func() {
		command.Album = "Album Name"
		Expect(command.DockerCommand().String()).To(Equal("eyeD3 --album Album Name some-file.mp3"))
	})

	It("should correctly generate cli command with Year", func() {
		command.Year = 2023
		Expect(command.DockerCommand().String()).To(Equal("eyeD3 --release-year 2023 some-file.mp3"))
	})

	It("should correctly generate cli command with genre", func() {
		command.Genre = "Genre Name"
		Expect(command.DockerCommand().String()).To(Equal("eyeD3 --genre Genre Name some-file.mp3"))
	})

	It("should correctly generate cli command with lyric file", func() {
		command.LyricsFile = "lyric-file.lrc"
		Expect(command.DockerCommand().String()).To(Equal("eyeD3 --add-lyrics lyric-file.lrc some-file.mp3"))
	})

	It("should correctly generate cli command with track Numbers", func() {
		command.TrackNumber = 1
		command.TrackTotalNumber = 10
		Expect(command.DockerCommand().String()).To(Equal("eyeD3 --track 1 --track-total 10 some-file.mp3"))
	})
})
