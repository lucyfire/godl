package metadata_test

import (
	"fmt"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmd/audio/metadata"
)

var _ = Describe("MetadataCommand validation", func() {
	var command metadata.Command
	BeforeEach(func() {
		command = metadata.Command{
			FilePath:         "some-file.mp3",
			TrackNumber:      -1,
			TrackTotalNumber: -1,
		}
	})

	It("should correctly validate", func() {
		command.Year = 2023
		command.TrackNumber = 2
		command.TrackTotalNumber = 3
		err := command.Validate()
		Expect(err).To(BeNil())
	})

	It("should fail if no arguments are passed", func() {
		err := command.Validate()
		Expect(err).To(Equal(fmt.Errorf("atleast one flag required")))
	})

	It("should fail if negative year is passed", func() {
		command.Year = -2023
		err := command.Validate()
		Expect(err).To(Equal(fmt.Errorf("year flag can't be negative number")))
	})

	It("should fail if negative track number is passed", func() {
		command.TrackNumber = -2
		err := command.Validate()
		Expect(err).To(Equal(fmt.Errorf("track number should be 0 or greater")))
	})

	It("should fail if negative track total number is passed", func() {
		command.TrackNumber = 2
		command.TrackTotalNumber = 1
		err := command.Validate()
		Expect(err).To(Equal(fmt.Errorf("total number of tracks should be equal or greater than the provided track number")))
	})
})
