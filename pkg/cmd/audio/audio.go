package audio

import (
	"errors"
	"net/url"

	"gitlab.com/lucyfire/godl/pkg/runner"

	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/godl/pkg/cmd/audio/chop"
	"gitlab.com/lucyfire/godl/pkg/cmd/audio/metadata"
	"gitlab.com/lucyfire/godl/pkg/cmd/audio/thumbnail"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

func NewCmdAudio(f *cmdutil.Factory) *cobra.Command {
	realCmd := &Command{}
	cmd := &cobra.Command{
		Use:     "audio { <url> }",
		Short:   "Downloads url as mp3",
		Example: "$ godl audio https://www.youtube.com/watch?v=dQw4w9WgXcQ [ --keep-thumbnail | --output output-name ]",
		Args: func(cmd *cobra.Command, args []string) error {
			var err error
			if err = cobra.ExactArgs(1)(cmd, args); err != nil {
				return err
			}
			if realCmd.URL, err = url.ParseRequestURI(args[0]); err != nil {
				return errors.New("url provided is not a valid url")
			}
			return nil
		},
		Run: func(cmd *cobra.Command, _ []string) {
			runner.RunCommand(cmd, f, realCmd)
		},
	}

	cmdutil.AddGroup(cmd, "General commands",
		thumbnail.NewCmdThumbnail(f),
		chop.NewCmdChop(f),
		metadata.NewCmdMetadata(f),
	)

	cmd.Flags().StringVarP(&realCmd.Format, "format", "f", "", "The format to download. (Defaults to the best audio)")
	cmd.Flags().BoolVar(&realCmd.KeepThumbnail, "keep-thumbnail", false, "Keep thumbnail as a separate file")
	cmd.Flags().StringVarP(&realCmd.Output, "output", "o", "", "The output filename. (Defaults to the page title)")
	cmd.Flags().BoolVar(&realCmd.ListFormats, "list-formats", false, "Lists the available formats of the url")
	return cmd
}
