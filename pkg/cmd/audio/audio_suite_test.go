package audio_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestAudioCommand(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Audio Command Suite")
}
