package audio

import (
	"net/url"

	"gitlab.com/lucyfire/docker"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

type Command struct {
	URL           *url.URL
	Output        string
	Format        string
	KeepThumbnail bool
	ListFormats   bool
}

func (c *Command) ContainerConfig() docker.ContainerConfig {
	return docker.ContainerConfig{
		Image:       cmdutil.DockerImage,
		Labels:      map[string]string{"creator": "godl"},
		Directories: cmdutil.PathsToMounts(),
		Command:     c.DockerCommand(),
		AutoRemove:  true,
	}
}

func (c *Command) DockerCommand() docker.Command {
	cmd := docker.Command{
		"yt-dlp",
		"--extract-audio",
		"--embed-thumbnail",
	}
	if c.Output == "" {
		c.Output = "%(title)s"
	}
	if c.KeepThumbnail {
		cmd = append(cmd, "--write-thumbnail")
	}
	if c.ListFormats {
		cmd = append(cmd, "--list-formats")
	}
	if c.Format != "" {
		cmd = append(cmd, "--format", c.Format)
	}

	return append(cmd, []string{
		"--audio-format", "mp3",
		"--output", c.Output + ".%(ext)s",
		c.URL.String(),
	}...)
}
