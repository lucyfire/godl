package spotify

import (
	"errors"
	"net/url"

	"gitlab.com/lucyfire/godl/pkg/runner"

	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

func NewCmdSpotify(f *cmdutil.Factory) *cobra.Command {
	realCmd := &Command{}
	cmd := &cobra.Command{
		Use:     "spotify <spotify-url>",
		Short:   "Downloads metadata from spotify and the music file from youtube",
		Example: "$ godl spotify https://open.spotify.com/track/4cOdK2wGLETKBW3PvgPWqT",
		Args: func(cmd *cobra.Command, args []string) error {
			var err error
			if err = cobra.ExactArgs(1)(cmd, args); err != nil {
				return err
			}
			if realCmd.URL, err = url.ParseRequestURI(args[0]); err != nil {
				return errors.New("url provided is not a valid url")
			}
			return nil
		},
		Run: func(cmd *cobra.Command, _ []string) {
			runner.RunCommand(cmd, f, realCmd)
		},
	}

	return cmd
}
