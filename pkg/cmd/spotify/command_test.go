package spotify_test

import (
	"net/url"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmd/spotify"
)

var _ = Describe("Spotify Command", func() {
	var command spotify.Command
	BeforeEach(func() {
		command = spotify.Command{}
		command.URL, _ = url.ParseRequestURI("https://some-url.com")
	})

	It("should correctly generate cli command", func() {
		Expect(command.DockerCommand().String()).To(Equal("spotdl download https://some-url.com"))
	})
})
