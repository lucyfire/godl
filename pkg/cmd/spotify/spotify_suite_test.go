package spotify_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestSpotifyCommand(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Spotify Command Suite")
}
