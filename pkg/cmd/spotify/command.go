package spotify

import (
	"net/url"

	"gitlab.com/lucyfire/docker"

	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

type Command struct {
	URL    *url.URL
	Output string
}

func (c *Command) ContainerConfig() docker.ContainerConfig {
	return docker.ContainerConfig{
		Image:       cmdutil.DockerImage,
		Labels:      map[string]string{"creator": "godl"},
		Directories: cmdutil.PathsToMounts(),
		Command:     c.DockerCommand(),
		AutoRemove:  true,
	}
}

func (c *Command) DockerCommand() docker.Command {
	return docker.Command{
		"spotdl",
		"download",
		c.URL.String(),
	}
}
