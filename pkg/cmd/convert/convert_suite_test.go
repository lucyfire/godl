package convert_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestConvertCommand(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Convert Command Suite")
}
