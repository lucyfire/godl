package convert

import (
	"errors"

	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

func (c *Command) Validate() error {
	if c.IgnoreFileValidations {
		return nil
	}
	var err error
	if c.isAudio, c.isVideo, err = cmdutil.IsFileAudioOrVideo(c.FilePath); err != nil || (!c.isVideo && !c.isAudio) {
		return errors.New("file should be an audio or video")
	}
	return nil
}
