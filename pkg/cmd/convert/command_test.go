package convert_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmd/convert"
)

var _ = Describe("Convert Command", func() {
	command := convert.Command{
		FilePath: "some-file.mp4",
		Format:   "mkv",
	}

	It("should correctly generate cli command", func() {
		Expect(command.DockerCommand().String()).To(Equal("ffmpeg -i some-file.mp4 some-file-converted.mkv"))
		command.Output = "output-file"
		Expect(command.DockerCommand().String()).To(Equal("ffmpeg -i some-file.mp4 output-file.mkv"))
	})
})
