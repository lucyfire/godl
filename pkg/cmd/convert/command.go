package convert

import (
	"path/filepath"
	"strings"

	"gitlab.com/lucyfire/docker"

	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

type Command struct {
	Output                string
	FilePath              string
	Format                string
	isAudio               bool
	isVideo               bool
	IgnoreFileValidations bool
}

func (c *Command) ContainerConfig() docker.ContainerConfig {
	return docker.ContainerConfig{
		Image:       cmdutil.DockerImage,
		Labels:      map[string]string{"creator": "godl"},
		Directories: cmdutil.PathsToMounts(c.FilePath),
		Command:     c.DockerCommand(),
		AutoRemove:  true,
	}
}

func (c *Command) DockerCommand() docker.Command {
	if c.Output == "" {
		basename := filepath.Base(c.FilePath)
		c.Output = strings.TrimSuffix(basename, filepath.Ext(basename)) + "-converted"
	}
	return docker.Command{
		"ffmpeg",
		"-i", c.FilePath,
		c.Output + "." + c.Format,
	}
}
