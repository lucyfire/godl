package convert

import (
	"errors"

	"github.com/AlecAivazis/survey/v2"
	"github.com/MakeNowJust/heredoc"
	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/runner"
	"gitlab.com/lucyfire/godl/pkg/types"
)

func NewCmdConvert(f *cmdutil.Factory) *cobra.Command {
	realCmd := &Command{}
	cmd := &cobra.Command{
		Use:     "convert { <audio-file> | <video-file> }",
		Short:   "Converts an audio/video file to another format",
		Example: "$ godl convert { video-file.mp4 | audio-file.mp3 } [ --output output-name ]",
		Long: heredoc.Doc(`
			Re encodes a file using ffmpeg into another format, you will be prompted with
			valid formats depended on the input file.
		`),
		Args: func(cmd *cobra.Command, args []string) error {
			if err := cobra.ExactArgs(1)(cmd, args); err != nil {
				return err
			}
			realCmd.FilePath = args[0]
			if err := cmdutil.CanonicalizePaths(&realCmd.FilePath); err != nil {
				return errors.New("failed to canonicalize file paths")
			}
			if f.IgnoreValidations {
				return nil
			}
			return realCmd.Validate()
		},
		PreRunE: func(_ *cobra.Command, _ []string) error {
			var formats types.SliceType
			switch {
			case realCmd.isAudio:
				formats = types.AudioMimeTypes()
			case realCmd.isVideo:
				formats = types.VideoMimeTypes()
			}
			return survey.AskOne(&survey.Select{
				Message: "Choose a new format:",
				Options: formats.Strings(),
			}, &realCmd.Format, survey.WithValidator(survey.Required))
		},
		Run: func(cmd *cobra.Command, _ []string) {
			runner.RunCommand(cmd, f, realCmd)
		},
	}

	cmd.Flags().StringVarP(&realCmd.Output, "output", "o", "", "The output filename")
	cmd.Flags().BoolVar(&realCmd.IgnoreFileValidations, "ignore-file-validations", false, "Ignore any file type validations")
	return cmd
}
