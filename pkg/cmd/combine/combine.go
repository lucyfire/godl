package combine

import (
	"errors"

	"github.com/MakeNowJust/heredoc"
	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/runner"
)

func NewCmdCombine(f *cmdutil.Factory) *cobra.Command {
	realCmd := &Command{}
	cmd := &cobra.Command{
		Use:     "combine",
		Short:   "Combines a video and audio file",
		Example: "$ godl combine --video video-file.mp4 --audio audio-file.mp3 [ --output output-name | --ignore-file-validations ]",
		Args: func(_ *cobra.Command, _ []string) error {
			if err := cmdutil.CanonicalizePaths(&realCmd.VideoFilePath, &realCmd.AudioFilePath); err != nil {
				return errors.New("failed to canonicalize file paths")
			}
			if f.IgnoreValidations {
				return nil
			}
			return realCmd.Validate()
		},
		Long: heredoc.Doc(`
			Combines a video and audio file into a new file. The audio will override all
			existing audio in the video file. Outputs combined file keeping the extension of
			the video file.
		`),
		Run: func(cmd *cobra.Command, _ []string) {
			runner.RunCommand(cmd, f, realCmd)
		},
	}

	cmd.Flags().StringVarP(&realCmd.Output, "output", "o", "", "The output filename")
	cmd.Flags().StringVarP(&realCmd.VideoFilePath, "video", "v", "", "The video file")
	cmd.Flags().StringVarP(&realCmd.AudioFilePath, "audio", "a", "", "The audio file")
	cmd.Flags().BoolVar(&realCmd.IgnoreFileValidations, "ignore-file-validations", false, "Ignore any file type validations")
	cmd.MarkFlagRequired("video")
	cmd.MarkFlagRequired("audio")

	return cmd
}
