package combine_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestCombineCommand(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Combine Command Suite")
}
