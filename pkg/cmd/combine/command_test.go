package combine_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmd/combine"
)

var _ = Describe("Combine Command", func() {
	command := combine.Command{
		VideoFilePath: "some-file.mp4",
		AudioFilePath: "some-file.mp3",
	}

	It("should correctly generate cli command", func() {
		Expect(command.DockerCommand().String()).To(Equal("ffmpeg -i some-file.mp4 -i some-file.mp3 -c:v copy -c:a aac some-file-combined.mp4"))
		command.Output = "output-file"
		Expect(command.DockerCommand().String()).To(Equal("ffmpeg -i some-file.mp4 -i some-file.mp3 -c:v copy -c:a aac output-file.mp4"))
	})
})
