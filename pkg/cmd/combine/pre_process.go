package combine

import (
	"errors"

	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

func (c *Command) Validate() error {
	if c.IgnoreFileValidations {
		return nil
	}
	if isAudio, err := cmdutil.IsFileAudio(c.AudioFilePath); err != nil || !isAudio {
		return errors.New("audio file provided isn't in an audio format")
	}
	if isVideo, err := cmdutil.IsFileVideo(c.VideoFilePath); err != nil || !isVideo {
		return errors.New("video provided isn't in a video format")
	}
	return nil
}
