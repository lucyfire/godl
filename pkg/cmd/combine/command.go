package combine

import (
	"path/filepath"
	"strings"

	"gitlab.com/lucyfire/docker"

	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

type Command struct {
	Output                string
	VideoFilePath         string
	AudioFilePath         string
	IgnoreFileValidations bool
}

func (c *Command) ContainerConfig() docker.ContainerConfig {
	return docker.ContainerConfig{
		Image:       cmdutil.DockerImage,
		Labels:      map[string]string{"creator": "godl"},
		Directories: cmdutil.PathsToMounts(c.VideoFilePath, c.AudioFilePath),
		Command:     c.DockerCommand(),
		AutoRemove:  true,
	}
}

func (c *Command) DockerCommand() docker.Command {
	if c.Output == "" {
		basename := filepath.Base(c.VideoFilePath)
		c.Output = strings.TrimSuffix(basename, filepath.Ext(basename)) + "-combined"
	}
	return docker.Command{
		"ffmpeg",
		"-i", c.VideoFilePath,
		"-i", c.AudioFilePath,
		"-c:v", "copy",
		"-c:a", "aac",
		c.Output + filepath.Ext(c.VideoFilePath),
	}
}
