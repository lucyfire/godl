package docker

import (
	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

func NewCmdDocker(f *cmdutil.Factory) *cobra.Command {
	realCmd := &Command{}
	cmd := &cobra.Command{
		Use:     "docker",
		Short:   "Operations on all docker containers spawned by godl",
		Example: "$ godl docker [ --kill | --show-output ]",
		Run: func(_ *cobra.Command, _ []string) {
			realCmd.Run(f)
		},
	}

	cmd.Flags().BoolVar(&realCmd.Kill, "kill", false, "Kills the spawned containers")
	cmd.Flags().BoolVar(&realCmd.ShowOutput, "show-output", false, "Shows the output of all containers")
	cmd.MarkFlagsMutuallyExclusive("kill", "show-output")

	return cmd
}
