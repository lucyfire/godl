package docker

import (
	"context"
	"fmt"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"go.uber.org/zap"
)

type Command struct {
	Kill       bool
	ShowOutput bool
}

func (c *Command) Run(f *cmdutil.Factory) {
	if c.Kill {
		c.killContainers(context.Background(), f)
		return
	}

	if c.ShowOutput {
		f.Logger.Warn("This is an experimental feature")
		c.showInfo(context.Background())
	}
}

func (c *Command) showInfo(ctx context.Context) {
	cli, _ := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	containers, _ := c.getContainers(ctx, cli)
	for i, cont := range containers {
		fmt.Printf(`%d: "%s" with command [%s]%c`, i+1, cont.Names, cont.Command, '\n')
	}
}

func (c *Command) killContainers(ctx context.Context, f *cmdutil.Factory) {
	cli, _ := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	containers, _ := c.getContainers(ctx, cli)
	for _, cont := range containers {
		f.Logger.Debug("killing container",
			zap.String("container", cont.Names[0]),
			zap.String("command", cont.Command),
		)
		if err := cli.ContainerStop(ctx, cont.ID, container.StopOptions{Signal: "SIGKILL"}); err != nil {
			f.Logger.Error("Failed to stop container",
				zap.String("container", cont.Names[0]),
				zap.Error(err),
			)
		}
	}
}

func (*Command) getContainers(ctx context.Context, cli *client.Client) ([]types.Container, error) {
	return cli.ContainerList(ctx, container.ListOptions{
		Filters: filters.NewArgs(filters.KeyValuePair{
			Key:   "label",
			Value: "creator=godl",
		}),
	})
}

// Experimental
// func (c *DockerClient) ShowOutput(ctx context.Context) {
// 	containers, _ := c.Client.ContainerList(ctx, types.ContainerListOptions{
// 		Filters: filters.NewArgs(filters.KeyValuePair{
// 			Key:   "label",
// 			Value: "creator=godl",
// 		}),
// 	})
// 	if len(containers) == 0 {
// 		return
// 	}
// 	w := uilive.New()
// 	var attaches []types.HijackedResponse
// 	for _, cont := range containers {
// 		attach, _ := c.Client.ContainerAttach(context.Background(), cont.ID, types.ContainerAttachOptions{
// 			Stream: true,
// 			Stdout: true,
// 			Stderr: true,
// 		})
// 		attaches = append(attaches, attach)
// 	}
// 	b := make([]byte, 1024)
// 	for {
// 		outputExists := false
// 		for _, attach := range attaches {
// 			read, err := attach.Reader.Read(b)
// 			if (err == nil || err == io.EOF) && read > 0 {
// 				outputExists = true
// 				w2 := w.Newline()
// 				w2.Write(b[:read])
// 				fmt.Fprintf(w2, "  %d \n", read)
// 			} else {
// 				attach.Close()
// 			}
// 		}
// 		time.Sleep(500 * time.Millisecond)
// 		w.Flush()
// 		if !outputExists {
// 			break
// 		}
// 	}
// }
