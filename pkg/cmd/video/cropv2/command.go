package cropv2

import (
	"fmt"
	"image"

	"gitlab.com/lucyfire/docker/v2"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

type Command struct {
	File            *cmdutil.InputFile
	Output          string
	FinalOutputName string
	DockerConfig    docker.ContainerConfig
	CropStart       image.Point
	Width           int
	Height          int
}

func (c *Command) ContainerConfig() docker.ContainerConfig {
	return c.DockerConfig
}

func (c *Command) Prepare() error {
	if c.Output == "" {
		c.Output = c.File.BaseName + "-cropped"
	}
	c.FinalOutputName = c.Output + c.File.Ext
	return nil
}

func (c *Command) Command() []string {
	return []string{
		"ffmpeg",
		"-hide_banner",
		"-loglevel", "fatal",
		"-progress", "-",
		"-i", c.File.Path,
		"-filter_complex",
		fmt.Sprintf("[0:v]crop=w=%d:h=%d:x=%d:y=%d", c.Width, c.Height, c.CropStart.X, c.CropStart.Y),
		c.FinalOutputName,
	}
}
