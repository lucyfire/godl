package cropv2

import (
	"os"

	"github.com/MakeNowJust/heredoc"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/docker/v2"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/runner"
	"gitlab.com/lucyfire/lucytea/navstack"
)

func NewCmdCropV2(f *cmdutil.Factory) *cobra.Command {
	realCmd := &Command{
		File: cmdutil.NewInputFile(cmdutil.VideoFile),
		DockerConfig: docker.ContainerConfig{
			Image:    cmdutil.DockerImage,
			Labels:   cmdutil.DockerLabels,
			IOStream: docker.StdIOStream,
			TTY:      true,
		},
	}
	cmd := &cobra.Command{
		Use:   "cropv2 { <video-file> }",
		Short: "Crops a video file",
		Long:  "Crops an area of width x height starting from position X x Y",
		Annotations: map[string]string{
			cmdutil.PathFfmpeg:                   "true",
			cmdutil.PathFfprobe:                  "true",
			cmdutil.AnnotationFlagDocker:         "true",
			cmdutil.AnnotationFlagInteractive:    "true",
			cmdutil.AnnotationFlagFileConstrains: "true",
			cmdutil.AnnotationFlagFileOutput:     "true",
		},
		Example: heredoc.Doc(`
			# Crops a 100 by 100 pixels starting from top left
			$ godl video cropv2 video-file.mp4 \ 
				--width  100 --height  100 \
				--x-position 0 --y-position 0 \
				[ --output output-name | --ignore-file-validations ]
			# Interactive way to crop video
			$ godl video cropv2 video-file.mp4 --interactive 
		`),
		Args: validate(f, realCmd),
		RunE: run(f, realCmd),
	}

	cmd.Flags().IntVarP(&realCmd.Width, "width", "w", 0, "The width of the cropped area")
	cmd.Flags().IntVarP(&realCmd.Height, "height", "h", 0, "The height of the cropped area")
	cmd.Flags().IntVarP(&realCmd.CropStart.X, "x-position", "x", 0, "The x position the crop area will start")
	cmd.Flags().IntVarP(&realCmd.CropStart.Y, "y-position", "y", 0, "The y position the crop area will start")
	cmd.MarkFlagsRequiredTogether("width", "height", "x-position", "y-position")
	return cmd
}

func validate(f *cmdutil.Factory, realCmd *Command) func(cmd *cobra.Command, args []string) error {
	return func(cmd *cobra.Command, args []string) error {
		cmd.MarkFlagsMutuallyExclusive(cmdutil.FlagInteractive, "width")
		cmd.MarkFlagsOneRequired(cmdutil.FlagInteractive, "width")
		if err := cobra.ExactArgs(1)(cmd, args); err != nil {
			return err
		}
		realCmd.File.Path = args[0]
		if err := realCmd.File.Canonicalize(); err != nil {
			return err
		}

		if err := f.ValidateAnnotations(cmd); err != nil {
			return err
		}
		if f.IgnoreValidations || f.IgnoreFileConstrains {
			return nil
		}
		return realCmd.File.ValidateAnd()
	}
}

func run(f *cmdutil.Factory, realCmd *Command) func(cmd *cobra.Command, args []string) error {
	return func(cmd *cobra.Command, args []string) error {
		realCmd.Output = f.Output
		// Execute the command in docker.
		if f.ExecuteInDocker && !f.InsideDocker {
			realCmd.DockerConfig.WorkingDir, _ = os.Getwd()
			realCmd.DockerConfig.Command = cmdutil.DockerfyCommand(cmd, []string{realCmd.File.Path})
			realCmd.DockerConfig.Directories = cmdutil.PathsToMounts(realCmd.File.Path)
			realCmd.DockerConfig.AutoRemove = f.Daemon
			return runner.StartDocker(cmd.Context(), f, realCmd)
		}
		// Cannot be interactive and daemon together.
		if f.Daemon && !f.Interactive {
			return runner.ExecuteDaemon(realCmd)
		}
		// if it's not running in daemon, we need tea, no matter if it's
		// interactive or not so runner.Execute should always be inside the
		// tea application.
		var err error
		m := newModel(f, realCmd)
		if f.Interactive {
			_, err = tea.NewProgram(navstack.New(m), tea.WithAltScreen()).Run()
		} else {
			_, err = tea.NewProgram(navstack.New(m)).Run()
		}
		if err != nil {
			return err
		}
		return m.Error()
	}
}
