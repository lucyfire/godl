package cropv2

import (
	"bytes"
	"image"
	"io"
	"os"

	tea "github.com/charmbracelet/bubbletea"
	"gitlab.com/lucyfire/godl/pkg/cmd/video/extractframe"
	"gitlab.com/lucyfire/godl/pkg/cmd/video/ffprobe"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/cmdutil/parser"
	"gitlab.com/lucyfire/godl/pkg/runner"
	"gitlab.com/lucyfire/lucytea/croparea"
	"gitlab.com/lucyfire/lucytea/navstack"
	"gitlab.com/lucyfire/lucytea/progress"
)

type executeCropMsg struct{}

type model struct {
	err            error
	FileFfprobe    *ffprobe.ProbeData
	Factory        *cmdutil.Factory
	cmd            *Command
	progress       *progress.Model
	crop           *croparea.Model
	loadingMessage string
	loading        bool
	init           bool
}

func (m *model) Init() tea.Cmd {
	if m.init {
		return nil
	}
	m.init = true
	return tea.Sequence(func() tea.Msg {
		m.loading = true
		m.loadingMessage = "extracting video data"
		return struct{}{}
	}, m.executeFfprobe, func() tea.Msg {
		if m.Factory.Interactive {
			return m.executeSelectCrop()
		}
		return executeCropMsg{}
	})
}

func (m *model) Loading() bool {
	return m.loading
}

func (m *model) LoadingMessage() string {
	return m.loadingMessage
}

func (m *model) Error() error {
	return m.err
}

func (m *model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		if msg.Type == tea.KeyCtrlC {
			m.Factory.Context.Cancel()
			return m, tea.Quit
		}
	case image.Rectangle:
		m.cmd.Height = msg.Size().Y
		m.cmd.Width = msg.Size().X
		m.cmd.CropStart = msg.Min
		return m, m.executeCrop()
	case executeCropMsg:
		return m, m.executeCrop()
	case error:
		m.err = msg
		return m, tea.Quit
	}
	return m, nil
}

func (m *model) executeFfprobe() tea.Msg {
	data, err := ffprobe.Ffprobe(m.Factory, m.cmd.File)
	if err != nil {
		return err
	}
	m.FileFfprobe = data
	m.loadingMessage = "finished execute ffprobe"
	return nil
}

func (m *model) executeSelectCrop() tea.Msg {
	videoStream := m.FileFfprobe.FirstVideoStream()
	extImgCmd := &extractframe.Command{
		File:             m.cmd.File,
		VideoStreamIndex: videoStream.Index,
		Width:            videoStream.Width,
		Height:           videoStream.Height,
	}
	buf2 := bytes.NewBuffer(nil)
	runner.Execute(m.Factory, extImgCmd, buf2, os.Stderr, os.Stdin)
	m.crop = croparea.New(buf2)
	m.loading = false
	return navstack.PushNavigable(m.crop)
}

// executeCrop runs the final crop command and renders the progress bar
func (m *model) executeCrop() tea.Cmd {
	_ = m.cmd.Prepare()
	r, w := io.Pipe()
	m.progress.Total = int(m.FileFfprobe.Format.Duration().Microseconds())
	executeCmd := func() tea.Msg {
		return runner.Execute(m.Factory, m.cmd, w, nil, nil)
	}

	finalCmd, _ := parser.HandleTeaProgress(parser.NewFfmpegParser(r), m.progress, r, executeCmd)
	return cmdutil.TeaFileExistsPrompt(
		m.cmd.FinalOutputName,
		tea.Sequence(finalCmd, tea.Quit),
	)
}

func (m *model) View() string {
	return m.progress.View()
}

func newModel(f *cmdutil.Factory, c *Command) *model {
	return &model{
		progress: progress.New(progress.WithFPS(12)),
		cmd:      c,
		Factory:  f,
	}
}
