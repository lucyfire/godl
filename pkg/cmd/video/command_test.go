package video_test

import (
	"net/url"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmd/video"
)

var _ = Describe("Video Command", func() {
	var command video.Command
	BeforeEach(func() {
		command = video.Command{}
		command.URL, _ = url.ParseRequestURI("https://some-url.com")
	})

	It("should correctly generate cli command", func() {
		Expect(command.DockerCommand().String()).To(Equal("yt-dlp --embed-thumbnail --output %(title)s.%(ext)s https://some-url.com"))
		command.Output = "output"
		Expect(command.DockerCommand().String()).To(Equal("yt-dlp --embed-thumbnail --output output.%(ext)s https://some-url.com"))
		command.KeepThumbnail = true
		Expect(command.DockerCommand().String()).To(Equal("yt-dlp --embed-thumbnail --write-thumbnail --output output.%(ext)s https://some-url.com"))
	})
})
