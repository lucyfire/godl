package video

import (
	"errors"
	"net/url"

	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/godl/pkg/cmd/video/concat"
	"gitlab.com/lucyfire/godl/pkg/cmd/video/crop"
	"gitlab.com/lucyfire/godl/pkg/cmd/video/cropv2"
	"gitlab.com/lucyfire/godl/pkg/cmd/video/encode"
	"gitlab.com/lucyfire/godl/pkg/cmd/video/extractaudio"
	"gitlab.com/lucyfire/godl/pkg/cmd/video/removeaudio"
	"gitlab.com/lucyfire/godl/pkg/cmd/video/removeduplicateframes"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/runner"
)

func NewCmdVideo(f *cmdutil.Factory) *cobra.Command {
	realCmd := &Command{}
	cmd := &cobra.Command{
		Use:     "video { <url> }",
		Short:   "Downloads url",
		Example: "$ godl video https://www.youtube.com/watch?v=dQw4w9WgXcQ [ --keep-thumbnail | --output output-name ]",
		Args: func(cmd *cobra.Command, args []string) error {
			var err error
			if err = cobra.ExactArgs(1)(cmd, args); err != nil {
				return err
			}
			if realCmd.URL, err = url.ParseRequestURI(args[0]); err != nil {
				return errors.New("url provided is not a valid url")
			}
			return nil
		},
		Run: func(cmd *cobra.Command, _ []string) {
			runner.RunCommand(cmd, f, realCmd)
		},
	}

	cmdutil.AddGroup(cmd, "General commands",
		crop.NewCmdCrop(f),
		cropv2.NewCmdCropV2(f),
		encode.NewCmdEncode(f),
		extractaudio.NewCmdExtractAudio(f),
		removeaudio.NewCmdRemoveAudio(f),
		removeduplicateframes.NewCmdRemoveDuplicateFrames(f),
		concat.NewCmdConcat(f),
	)

	cmd.Flags().StringVarP(&realCmd.Output, "output", "o", "", "The output filename. (Defaults to the page title)")
	cmd.Flags().StringVarP(&realCmd.Format, "format", "f", "", "The format to download. (Defaults to the best video and best audio)")
	cmd.Flags().BoolVar(&realCmd.KeepThumbnail, "keep-thumbnail", false, "Keep thumbnail as a separate file")
	cmd.Flags().BoolVar(&realCmd.ListFormats, "list-formats", false, "Lists the available formats of the url")
	return cmd
}
