package extractframe

import (
	"fmt"

	"gitlab.com/lucyfire/docker/v2"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

type Command struct {
	File             *cmdutil.InputFile
	Output           string
	VideoStreamIndex int
	Width            int
	Height           int
}

func (c *Command) ContainerConfig() docker.ContainerConfig {
	return docker.ContainerConfig{
		Image:       cmdutil.DockerImage,
		Labels:      cmdutil.DockerLabels,
		Directories: cmdutil.PathsToMounts(c.File.Path),
		Command:     docker.Command{"sleep", "infinity"},
		AutoRemove:  true,
	}
}

func (*Command) Prepare() error {
	return nil
}

func (c *Command) Command() []string {
	return []string{
		"ffmpeg", "-hide_banner",
		"-loglevel", "fatal",
		"-i", c.File.Path,
		"-filter_complex", fmt.Sprintf("[0:v:%d]scale=width=%d:height=%d[s0]", c.VideoStreamIndex, c.Width, c.Height),
		"-map", "[s0]",
		"-f", "mjpeg",
		"-vframes", "1",
		"pipe:1",
	}
}
