package removeduplicateframes_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestRemoveDuplicateFramesCommand(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Remove Duplicate Frames Command Suite")
}
