package removeduplicateframes

import (
	"path/filepath"
	"strings"

	"gitlab.com/lucyfire/docker"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

type Command struct {
	Output                string
	FilePath              string
	IgnoreFileValidations bool
}

func (c *Command) ContainerConfig() docker.ContainerConfig {
	return docker.ContainerConfig{
		Image:       cmdutil.DockerImage,
		Labels:      map[string]string{"creator": "godl"},
		Directories: cmdutil.PathsToMounts(c.FilePath),
		Command:     c.DockerCommand(),
		AutoRemove:  true,
	}
}

func (c *Command) DockerCommand() docker.Command {
	if c.Output == "" {
		basename := filepath.Base(c.FilePath)
		c.Output = strings.TrimSuffix(basename, filepath.Ext(basename)) + "-cleaned"
	}
	return docker.Command{
		"ffmpeg",
		"-i", c.FilePath,
		"-c", "copy",
		"-vf", "mpdecimate,setpts=N/FRAME_RATE/TB",
		c.Output + filepath.Ext(c.FilePath),
	}
}
