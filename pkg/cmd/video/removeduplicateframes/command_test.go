package removeduplicateframes_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmd/video/removeduplicateframes"
)

var _ = Describe("Remove Duplicate Frames Command", func() {
	command := removeduplicateframes.Command{
		FilePath: "some-file.mp4",
	}

	It("should correctly generate cli command", func() {
		Expect(command.DockerCommand().String()).To(Equal("ffmpeg -i some-file.mp4 -c copy -vf mpdecimate,setpts=N/FRAME_RATE/TB some-file-cleaned.mp4"))
		command.Output = "output-file"
		Expect(command.DockerCommand().String()).To(Equal("ffmpeg -i some-file.mp4 -c copy -vf mpdecimate,setpts=N/FRAME_RATE/TB output-file.mp4"))
	})
})
