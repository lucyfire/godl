package removeaudio

import (
	"errors"

	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

func (c *Command) Validate() error {
	if c.IgnoreFileValidations {
		return nil
	}
	if isVideo, err := cmdutil.IsFileVideo(c.FilePath); err != nil || !isVideo {
		return errors.New("file provided isn't in a video format")
	}
	return nil
}
