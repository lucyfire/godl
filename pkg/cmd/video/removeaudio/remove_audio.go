package removeaudio

import (
	"errors"

	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/runner"
)

func NewCmdRemoveAudio(f *cmdutil.Factory) *cobra.Command {
	realCmd := &Command{}
	cmd := &cobra.Command{
		Use:     "remove-audio { <video-file> }",
		Short:   "Removes audio from a video file",
		Long:    "Removes all audio channels from a video file",
		Example: "$ godl video remove-audio video-file.mp4 [ --output output-name | --ignore-file-validations ]",
		Args: func(cmd *cobra.Command, args []string) error {
			if err := cobra.ExactArgs(1)(cmd, args); err != nil {
				return err
			}
			realCmd.FilePath = args[0]
			if err := cmdutil.CanonicalizePaths(&realCmd.FilePath); err != nil {
				return errors.New("failed to canonicalize file paths")
			}
			if f.IgnoreValidations {
				return nil
			}
			return realCmd.Validate()
		},
		Run: func(cmd *cobra.Command, _ []string) {
			runner.RunCommand(cmd, f, realCmd)
		},
	}

	cmd.Flags().StringVarP(&realCmd.Output, "output", "o", "", "The output filename")
	cmd.Flags().BoolVar(&realCmd.IgnoreFileValidations, "ignore-file-validations", false, "Ignore any file type validations")
	return cmd
}
