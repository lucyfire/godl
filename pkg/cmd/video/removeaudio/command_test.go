package removeaudio_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmd/video/removeaudio"
)

var _ = Describe("Remove Audio Command", func() {
	command := removeaudio.Command{
		FilePath: "some-file.mp4",
	}

	It("should correctly generate cli command", func() {
		Expect(command.DockerCommand().String()).To(Equal("ffmpeg -i some-file.mp4 -c copy -an some-file-muted.mp4"))
		command.Output = "output-file"
		Expect(command.DockerCommand().String()).To(Equal("ffmpeg -i some-file.mp4 -c copy -an output-file.mp4"))
	})
})
