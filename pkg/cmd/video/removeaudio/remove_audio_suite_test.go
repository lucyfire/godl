package removeaudio_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestRemoveAudioCommand(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Remove Audio Command Suite")
}
