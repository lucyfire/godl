package ffprobe

import (
	"bytes"
	"encoding/json"

	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/runner"
)

func Ffprobe(f *cmdutil.Factory, file *cmdutil.InputFile) (*ProbeData, error) {
	ffprobeCmd := &Command{File: file}
	buf := bytes.NewBuffer(nil)
	err := runner.Execute(f, ffprobeCmd, buf, nil, nil)
	if err != nil {
		return nil, err
	}

	var videoData ProbeData
	if err := json.Unmarshal(buf.Bytes(), &videoData); err != nil {
		return nil, err
	}
	return &videoData, nil
}
