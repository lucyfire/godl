package ffprobe

import (
	"gitlab.com/lucyfire/docker/v2"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

type Command struct {
	File                  *cmdutil.InputFile
	Docker                bool
	IgnoreFileValidations bool
}

func (c *Command) ContainerConfig() docker.ContainerConfig {
	return docker.ContainerConfig{
		Image:       cmdutil.DockerImage,
		Labels:      cmdutil.DockerLabels,
		Directories: cmdutil.PathsToMounts(c.File.Path),
		Command:     docker.Command{"sleep", "infinity"},
		AutoRemove:  true,
	}
}

func (*Command) Prepare() error {
	return nil
}

func (c *Command) Command() []string {
	return []string{
		"ffprobe",
		"-hide_banner",
		"-loglevel", "fatal",
		"-print_format", "json",
		"-show_format",
		"-show_streams",
		"-show_chapters",
		"-show_entries", "format=duration",
		c.File.Path,
	}
}
