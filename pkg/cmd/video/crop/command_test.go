package crop_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmd/video/crop"
)

var _ = Describe("Crop Command", func() {
	command := crop.Command{
		FilePath: "some-file.mp4",
		Width:    100,
		Height:   100,
		X:        10,
		Y:        10,
	}

	It("should correctly generate cli command", func() {
		Expect(command.DockerCommand().String()).To(Equal("ffmpeg -hide_banner -loglevel fatal -progress - -i some-file.mp4 -filter_complex [0:v]crop=w=100:h=100:x=10:y=10 some-file-cropped.mp4"))
		command.Output = "output-file"
		Expect(command.DockerCommand().String()).To(Equal("ffmpeg -hide_banner -loglevel fatal -progress - -i some-file.mp4 -filter_complex [0:v]crop=w=100:h=100:x=10:y=10 output-file.mp4"))
	})
})
