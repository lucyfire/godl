package crop

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/AlecAivazis/survey/v2"
	tea "github.com/charmbracelet/bubbletea"
	"gitlab.com/lucyfire/docker"
	"gitlab.com/lucyfire/godl/pkg/cmd/video/ffprobe"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/lucytea/croparea"
)

func (c *Command) Execs(f *cmdutil.Factory, container docker.Container) []error {
	defer container.Kill(context.Background(), f.Docker)
	f.CleanupHandler()

	probeData, err := c.execProbeData(f, container)
	if err != nil {
		return []error{err}
	}

	var videoStream *ffprobe.Stream
	for _, s := range probeData.Streams {
		if s.CodecType == "video" && s.Disposition.Default == 1 {
			videoStream = s
		}
	}
	if videoStream == nil {
		return []error{errors.New("videostream not found")}
	}

	img, err := c.execImage(f, container, videoStream)
	if err != nil {
		return []error{err}
	}
	if c.Interactive {
		m := croparea.New(img)
		_, err = tea.NewProgram(m, tea.WithAltScreen(), tea.WithoutSignals()).Run()
		if err != nil {
			return []error{err}
		}
		cropArea := m.CropCoordinates()

		c.Height = cropArea.Size().Y
		c.Width = cropArea.Size().X
		c.X = cropArea.Min.X
		c.Y = cropArea.Min.Y
	}

	if err := c.execCrop(f, container, videoStream); err != nil {
		return []error{err}
	}
	return nil
}

func (c *Command) execProbeData(f *cmdutil.Factory, container docker.Container) (*ffprobe.ProbeData, error) {
	buf := bytes.NewBuffer(nil)
	w, _ := docker.NewWrapper(docker.WithLogger(f.Logger), docker.WithOutputStream(buf), docker.WithInputStream(nil))

	e, err := container.Exec(f.Context.Ctx, w, docker.Command{
		"ffprobe",
		"-loglevel", "fatal",
		"-print_format", "json",
		"-show_format",
		"-show_streams",
		"-show_chapters",
		c.FilePath,
	})
	if err != nil {
		return nil, err
	}
	ios, err := e.AttachIO(f.Context.Ctx, w)
	if err != nil {
		return nil, err
	}
	if err = ios.Wait(f.Context.Ctx); err != nil {
		return nil, err
	}

	data := &ffprobe.ProbeData{}
	if err := json.Unmarshal(buf.Bytes(), data); err != nil {
		return nil, err
	}
	return data, nil
}

func (c *Command) execImage(f *cmdutil.Factory, container docker.Container, videoStream *ffprobe.Stream) (io.Reader, error) {
	buf := bytes.NewBuffer(nil)
	w, _ := docker.NewWrapper(docker.WithLogger(f.Logger), docker.WithOutputStream(buf), docker.WithInputStream(nil))

	e, err := container.ExecWithoutTTY(f.Context.Ctx, w, docker.Command{
		"ffmpeg", "-hide_banner",
		"-loglevel", "fatal",
		"-i", c.FilePath,
		"-filter_complex", fmt.Sprintf("[0:v:%d]scale=width=%d:height=%d[s0]", videoStream.Index, videoStream.Width, videoStream.Height),
		"-map", "[s0]",
		"-f", "mjpeg",
		"-vframes", "1",
		"pipe:1",
	})

	ios, err := e.AttachIO(f.Context.Ctx, w)
	if err != nil {
		return nil, err
	}
	if err = ios.Wait(f.Context.Ctx); err != nil {
		return nil, err
	}
	return buf, nil
}

func (c *Command) execCrop(f *cmdutil.Factory, container docker.Container, videoStream *ffprobe.Stream) error {
	cmd := c.DockerCommand()
	outputFile := c.Output + filepath.Ext(c.FilePath)
	if _, err := os.Stat(outputFile); err == nil {
		overwrite := false
		prompt := &survey.Confirm{
			Message: fmt.Sprintf("File '%s' already exists. Overwrite?", filepath.Base(outputFile)),
		}
		survey.AskOne(prompt, &overwrite)
		if !overwrite {
			return nil
		}
		if err := os.Remove(outputFile); err != nil {
			return err
		}
	}
	reader, writer := io.Pipe()
	w, _ := docker.NewWrapper(docker.WithLogger(f.Logger), docker.WithOutputStream(writer), docker.WithInputStream(nil))
	e, err := container.ExecWithoutTTY(f.Context.Ctx, w, cmd)
	if err != nil {
		return err
	}
	ios, err := e.AttachIO(f.Context.Ctx, w)
	if err != nil {
		return err
	}

	// can show progress bar
	if frames, err := strconv.Atoi(videoStream.NbFrames); err == nil {
		c.showProgressBar(frames, reader)
		return err
	}

	return ios.Wait(f.Context.Ctx)
}

func (*Command) showProgressBar(frames int, reader io.Reader) {
	m := newModel()
	m.Total = frames
	m.FPS = 12
	go func() {
		scanner := bufio.NewScanner(reader)
		for scanner.Scan() {
			parts := strings.SplitN(scanner.Text(), "=", 2)
			if len(parts) != 2 {
				continue
			}
			key, value := parts[0], parts[1]
			if key == "frame" {
				atoi, _ := strconv.Atoi(value)
				m.At = atoi
			}
		}
		m.At = m.Total
	}()
	tea.NewProgram(m, tea.WithoutSignals()).Run()
}
