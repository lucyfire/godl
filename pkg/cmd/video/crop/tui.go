package crop

import (
	"time"

	"github.com/charmbracelet/bubbles/progress"
	tea "github.com/charmbracelet/bubbletea"
)

type tickMsg time.Time

type model struct {
	progress progress.Model
	Total    int
	At       int
	FPS      int
	percent  float64
}

func (m *model) Init() tea.Cmd {
	return m.tick()
}

func (m *model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		if msg.Type == tea.KeyCtrlC {
			return m, tea.Quit
		}

	case tea.WindowSizeMsg:
		m.progress.Width = msg.Width - 4
		if m.progress.Width > 80 {
			m.progress.Width = 80
		}
		return m, nil

	case tickMsg:
		m.percent = float64(m.At) / float64(m.Total)
		if m.percent >= 1.0 {
			return m, tea.Quit
		}
		return m, m.tick()
	}
	return m, nil
}

func (m *model) View() string {
	return m.progress.ViewAs(m.percent)
}

func (m *model) tick() tea.Cmd {
	return tea.Tick(time.Second/time.Duration(m.FPS), func(t time.Time) tea.Msg {
		return tickMsg(t)
	})
}

func newModel() *model {
	return &model{
		progress: progress.New(),
		FPS:      4,
	}
}
