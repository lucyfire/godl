package crop

import (
	"errors"

	"github.com/MakeNowJust/heredoc"
	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/runner"
)

func NewCmdCrop(f *cmdutil.Factory) *cobra.Command {
	realCmd := &Command{}
	cmd := &cobra.Command{
		Use:   "crop { <video-file> }",
		Short: "Crops a video file",
		Long:  "Crops an area of width x height starting from position X x Y",
		Example: heredoc.Doc(`
			# Crops a 100 by 100 pixels starting from top left
			$ godl video crop video-file.mp4 \ 
				--width  100 --height  100 \
				--x-position 0 --y-position 0 \
				[ --output output-name | --ignore-file-validations ]
		`),
		Args: func(cmd *cobra.Command, args []string) error {
			if err := cobra.ExactArgs(1)(cmd, args); err != nil {
				return err
			}
			realCmd.FilePath = args[0]
			if err := cmdutil.CanonicalizePaths(&realCmd.FilePath); err != nil {
				return errors.New("failed to canonicalize file paths")
			}
			if f.IgnoreValidations {
				return nil
			}
			return realCmd.Validate()
		},
		Run: func(cmd *cobra.Command, _ []string) {
			f.Logger.Warn("crop command will be replaced with cropv2 command in the next major release")
			runner.RunCommandWithStartedContainer(cmd, f, realCmd)
		},
	}

	cmd.Flags().BoolVar(&realCmd.Interactive, "interactive", false, "Visually choose the crop area")
	cmd.Flags().IntVarP(&realCmd.Width, "width", "w", 0, "The width of the cropped area")
	cmd.Flags().IntVarP(&realCmd.Height, "height", "h", 0, "The height of the cropped area")
	cmd.Flags().IntVarP(&realCmd.X, "x-position", "x", 0, "The x position the crop area will start")
	cmd.Flags().IntVarP(&realCmd.Y, "y-position", "y", 0, "The y position the crop area will start")
	cmd.Flags().StringVarP(&realCmd.Output, "output", "o", "", "The output filename")
	cmd.Flags().BoolVar(&realCmd.IgnoreFileValidations, "ignore-file-validations", false, "Ignore any file type validations")
	cmd.MarkFlagsRequiredTogether("width", "height", "x-position", "y-position")
	cmd.MarkFlagsMutuallyExclusive("interactive", "width")
	cmd.MarkFlagsOneRequired("interactive", "width")

	return cmd
}
