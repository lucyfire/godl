package crop_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestCropCommand(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Crop Command Suite")
}
