package crop

import (
	"fmt"
	"path/filepath"
	"strings"

	"gitlab.com/lucyfire/docker"

	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

type Command struct {
	Output                string
	FilePath              string
	Width                 int
	Height                int
	X                     int
	Y                     int
	IgnoreFileValidations bool
	Interactive           bool
}

func (c *Command) ContainerConfig() docker.ContainerConfig {
	return docker.ContainerConfig{
		Image:       cmdutil.DockerImage,
		Labels:      map[string]string{"creator": "godl"},
		Directories: cmdutil.PathsToMounts(c.FilePath),
		Command:     docker.Command{"sleep", "infinity"},
		AutoRemove:  true,
	}
}

func (c *Command) DockerCommand() docker.Command {
	if c.Output == "" {
		basename := filepath.Base(c.FilePath)
		c.Output = strings.TrimSuffix(basename, filepath.Ext(basename)) + "-cropped"
	}
	return docker.Command{
		"ffmpeg",
		"-hide_banner",
		"-loglevel", "fatal",
		"-progress", "-",
		"-i", c.FilePath,
		"-filter_complex",
		fmt.Sprintf("[0:v]crop=w=%d:h=%d:x=%d:y=%d", c.Width, c.Height, c.X, c.Y),
		c.Output + filepath.Ext(c.FilePath),
	}
}
