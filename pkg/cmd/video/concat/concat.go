package concat

import (
	"os"

	"github.com/MakeNowJust/heredoc"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/docker/v2"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/runner"
	"gitlab.com/lucyfire/lucytea/navstack"
)

func NewCmdConcat(f *cmdutil.Factory) *cobra.Command {
	realCmd := &Command{
		Factory: f,
		DockerConfig: docker.ContainerConfig{
			Image:    cmdutil.DockerImage,
			Labels:   cmdutil.DockerLabels,
			IOStream: docker.StdIOStream,
			TTY:      true,
		},
	}
	cmd := &cobra.Command{
		Use:   "concat",
		Short: "Concatenates multiple video files into a single file",
		Long: heredoc.Doc(`
			Concatenates multiple videos into a single file, the new file will have
			the max width/height across all the videos combined. Final file is re-encoded
			to x264 in the same container as the first video.
		`),
		Annotations: map[string]string{
			cmdutil.PathFfmpeg:                   "true",
			cmdutil.PathFfprobe:                  "true",
			cmdutil.AnnotationFlagDocker:         "true",
			cmdutil.AnnotationFlagFileConstrains: "true",
			cmdutil.AnnotationFlagFileOutput:     "true",
		},
		Args: validate(f, realCmd),
		Example: heredoc.Doc(`
			$ godl concat video-1.mp4 video-2.mp4 video-3.mp4
		`),
		RunE: func(cmd *cobra.Command, args []string) error {
			realCmd.Output = f.Output
			// Execute the command in docker.
			if f.ExecuteInDocker && !f.InsideDocker {
				realCmd.DockerConfig.WorkingDir, _ = os.Getwd()
				realCmd.DockerConfig.Command = cmdutil.DockerfyCommand(cmd, args)
				var paths []string
				for _, file := range realCmd.Files {
					paths = append(paths, file.Path)
				}
				realCmd.DockerConfig.AutoRemove = f.Daemon
				realCmd.DockerConfig.Directories = cmdutil.PathsToMounts(paths...)
				return runner.StartDocker(cmd.Context(), f, realCmd)
			}
			if f.Daemon {
				return runner.ExecuteDaemon(realCmd)
			}
			m := newModel(f, realCmd)
			_, err := tea.NewProgram(navstack.New(m)).Run()
			if err != nil {
				return err
			}
			return m.Error()
		},
	}

	return cmd
}

func validate(f *cmdutil.Factory, realCmd *Command) func(cmd *cobra.Command, args []string) error {
	return func(cmd *cobra.Command, args []string) error {
		if err := cobra.MinimumNArgs(2)(cmd, args); err != nil {
			return err
		}

		if err := f.ValidateAnnotations(cmd); err != nil {
			return err
		}

		for _, arg := range args {
			file := cmdutil.NewInputFile(cmdutil.VideoFile)
			file.Path = arg
			if err := file.Canonicalize(); err != nil {
				return err
			}

			realCmd.Files = append(realCmd.Files, file)
			if f.IgnoreValidations || f.IgnoreFileConstrains {
				continue
			}

			if err := file.ValidateOr(); err != nil {
				return err
			}
		}
		return nil
	}
}
