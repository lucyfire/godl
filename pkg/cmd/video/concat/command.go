package concat

import (
	"fmt"
	"strings"
	"sync"

	"gitlab.com/lucyfire/docker/v2"
	"gitlab.com/lucyfire/godl/pkg/cmd/video/ffprobe"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

type Command struct {
	Factory         *cmdutil.Factory
	Output          string
	FinalOutputName string
	Files           []*cmdutil.InputFile
	FfprobeFiles    []*ffprobe.ProbeData
	DockerConfig    docker.ContainerConfig
	maxWidth        int
	maxHeight       int
	prepareOnce     sync.Once
}

func (c *Command) Prepare() error {
	var prepareError error
	c.prepareOnce.Do(func() {
		if c.Output == "" {
			c.Output = "concatenated"
		}

		for _, file := range c.Files {
			data, err := ffprobe.Ffprobe(c.Factory, file)
			if err != nil {
				prepareError = err
				return
			}
			c.maxHeight = max(data.FirstVideoStream().Height, c.maxHeight)
			c.maxWidth = max(data.FirstVideoStream().Width, c.maxWidth)
			c.FfprobeFiles = append(c.FfprobeFiles, data)
		}
		c.FinalOutputName = c.Output + c.Files[0].Ext
	})
	return prepareError
}

func (c *Command) ContainerConfig() docker.ContainerConfig {
	return c.DockerConfig
}

func (c *Command) Command() []string {
	cmd := []string{
		"ffmpeg",
		"-hide_banner",
		"-loglevel", "fatal",
		"-progress", "-",
	}
	for _, f := range c.Files {
		cmd = append(cmd, "-i", f.Path)
	}
	cmd = append(cmd, "-filter_complex")
	var filter []string
	for index, data := range c.FfprobeFiles {
		filter = append(filter, fmt.Sprintf("[%d:v]pad=%d:%d:%d:%d:black[v%d];",
			index,
			c.maxWidth,
			c.maxHeight,
			(c.maxWidth-data.FirstVideoStream().Width)/2,
			(c.maxHeight-data.FirstVideoStream().Height)/2,
			index),
		)
	}
	for index := range c.Files {
		filter = append(filter, fmt.Sprintf("[v%d][%d:a]", index, index))
	}
	filter = append(filter, fmt.Sprintf("concat=n=%d:v=1:a=1[v][a]", len(c.Files)))
	cmd = append(cmd, strings.Join(filter, ""))
	return append(cmd, []string{
		"-map", "[v]",
		"-map", "[a]",
		"-c:v", "libx264",
		"-crf", "18",
		"-preset", "veryfast",
		c.FinalOutputName,
	}...)
}
