package concat

import (
	"fmt"
	"io"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/cmdutil/parser"
	"gitlab.com/lucyfire/godl/pkg/runner"
	"gitlab.com/lucyfire/lucytea/progress"
)

type model struct {
	Factory        *cmdutil.Factory
	cmd            *Command
	err            error
	progress       *progress.Model
	ffmpegProgress *parser.FfmpegProgress
	loadingMessage string
	loading        bool
	init           bool
}

func (m *model) Init() tea.Cmd {
	if m.init {
		return nil
	}
	m.init = true
	return tea.Sequence(func() tea.Msg {
		m.loading = true
		m.loadingMessage = "extracting video data"
		return struct{}{}
	}, func() tea.Msg {
		if err := m.cmd.Prepare(); err != nil {
			return err
		}
		for _, data := range m.cmd.FfprobeFiles {
			m.progress.Total += int(data.Format.Duration().Microseconds())
		}
		m.loading = false
		return struct{}{}
	}, m.execute())
}

func (m *model) Loading() bool {
	return m.loading
}

func (m *model) LoadingMessage() string {
	return m.loadingMessage
}

func (m *model) Error() error {
	return m.err
}

func (m *model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		if msg.Type == tea.KeyCtrlC {
			m.Factory.Context.Cancel()
			return m, tea.Quit
		}
	case error:
		m.err = msg
		return m, tea.Quit
	}
	_, cmd := m.progress.Update(msg)
	return m, cmd
}

func (m *model) View() string {
	if m.ffmpegProgress == nil {
		return ""
	}
	return lipgloss.JoinVertical(lipgloss.Left,
		lipgloss.JoinHorizontal(lipgloss.Top,
			fmt.Sprintf("Frame: %d, ", m.ffmpegProgress.Frame),
			fmt.Sprintf("Bitrate: %s, ", m.ffmpegProgress.BitRate),
			fmt.Sprintf("Dropped: %d, ", m.ffmpegProgress.DroppedFrames),
			fmt.Sprintf("Duplicate: %d", m.ffmpegProgress.DupFrames),
		),
		m.progress.View(),
	)
}

func (m *model) execute() tea.Cmd {
	r, w := io.Pipe()
	executeCmd := tea.Sequence(func() tea.Msg {
		return runner.Execute(m.Factory, m.cmd, w, nil, nil)
	}, tea.Quit)

	m.ffmpegProgress = &parser.FfmpegProgress{}
	finalCmd, progressChan := parser.HandleTeaProgress(parser.NewFfmpegParser(r), m.progress, r, executeCmd)
	go func() {
		for progress := range progressChan {
			if p, ok := progress.(*parser.FfmpegProgress); ok {
				m.ffmpegProgress = p
			}
		}
	}()

	return cmdutil.TeaFileExistsPrompt(
		m.cmd.FinalOutputName,
		finalCmd,
	)
}

func newModel(f *cmdutil.Factory, c *Command) *model {
	return &model{
		Factory:  f,
		cmd:      c,
		progress: progress.New(progress.WithFPS(12)),
	}
}
