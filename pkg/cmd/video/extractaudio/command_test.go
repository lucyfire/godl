package extractaudio_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmd/video/extractaudio"
	"gitlab.com/lucyfire/godl/pkg/cmdutil/flags"
	"gitlab.com/lucyfire/godl/pkg/types"
)

var _ = Describe("Extract Audio Command", func() {
	command := extractaudio.Command{
		FilePath: "some-file.mp4",
		Format:   flags.AudioMimeType(types.Mp3),
	}

	It("should correctly generate cli command", func() {
		Expect(command.DockerCommand().String()).To(Equal("ffmpeg -i some-file.mp4 -vn -acodec mp3 some-file-extracted.mp3"))
		command.Output = "output-file"
		Expect(command.DockerCommand().String()).To(Equal("ffmpeg -i some-file.mp4 -vn -acodec mp3 output-file.mp3"))
	})
})
