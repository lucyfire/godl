package extractaudio

import (
	"path/filepath"
	"strings"

	"gitlab.com/lucyfire/docker"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/cmdutil/flags"
)

type Command struct {
	Output                string
	Format                flags.AudioMimeType
	FilePath              string
	IgnoreFileValidations bool
}

func (c *Command) ContainerConfig() docker.ContainerConfig {
	return docker.ContainerConfig{
		Image:       cmdutil.DockerImage,
		Labels:      map[string]string{"creator": "godl"},
		Directories: cmdutil.PathsToMounts(c.FilePath),
		Command:     c.DockerCommand(),
		AutoRemove:  true,
	}
}

func (c *Command) DockerCommand() docker.Command {
	if c.Output == "" {
		basename := filepath.Base(c.FilePath)
		c.Output = strings.TrimSuffix(basename, filepath.Ext(basename)) + "-extracted"
	}
	return docker.Command{
		"ffmpeg",
		"-i", c.FilePath,
		"-vn",
		"-acodec", c.Format.Usage,
		c.Output + "." + c.Format.Usage,
	}
}
