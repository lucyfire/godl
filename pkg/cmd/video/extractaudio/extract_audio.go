package extractaudio

import (
	"errors"

	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/cmdutil/flags"
	"gitlab.com/lucyfire/godl/pkg/runner"
	"gitlab.com/lucyfire/godl/pkg/types"
)

func NewCmdExtractAudio(f *cmdutil.Factory) *cobra.Command {
	realCmd := &Command{Format: flags.AudioMimeType(types.Mp3)}
	cmd := &cobra.Command{
		Use:     "extract-audio { <video-file> }",
		Short:   "Extracts the audio from a video file",
		Long:    "Extracts the first audio channel from a video file into mp3",
		Example: "$ godl video extract-audio video-file.mp4 [ --output output-name | --ignore-file-validations ]",
		Args: func(cmd *cobra.Command, args []string) error {
			if err := cobra.ExactArgs(1)(cmd, args); err != nil {
				return err
			}
			realCmd.FilePath = args[0]
			if err := cmdutil.CanonicalizePaths(&realCmd.FilePath); err != nil {
				return errors.New("failed to canonicalize file paths")
			}
			if f.IgnoreValidations {
				return nil
			}
			return realCmd.Validate()
		},
		Run: func(cmd *cobra.Command, _ []string) {
			runner.RunCommand(cmd, f, realCmd)
		},
	}

	cmd.Flags().StringVarP(&realCmd.Output, "output", "o", "", "The output filename")
	cmd.Flags().VarP(&realCmd.Format, "format", "f", "The output format")
	cmd.RegisterFlagCompletionFunc("format", func(_ *cobra.Command, _ []string, _ string) ([]string, cobra.ShellCompDirective) {
		return types.AudioMimeTypes().Strings(), cobra.ShellCompDirectiveDefault
	})
	cmd.Flags().BoolVar(&realCmd.IgnoreFileValidations, "ignore-file-validations", false, "Ignore any file type validations")

	return cmd
}
