package extractaudio_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestExtractAudioCommand(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Extract Audio Command Suite")
}
