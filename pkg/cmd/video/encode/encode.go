package encode

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/lucyfire/godl/pkg/runner"

	"github.com/MakeNowJust/heredoc"
	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/cmdutil/flags"
	"gitlab.com/lucyfire/godl/pkg/types"
)

func NewCmdEncode(f *cmdutil.Factory) *cobra.Command {
	realCmd := &Command{VideoCodec: flags.VideoCodec(types.H264)}
	cmd := &cobra.Command{
		Use:   "encode { <video-file> }",
		Short: "Encodes a video file",
		Long: heredoc.Doc(`
			Re-encodes a video changing its [constant rate factor](https://trac.ffmpeg.org/wiki/Encode/H.264)
			and [video encoders](https://www.ffmpeg.org/ffmpeg-codecs.html#Video-Encoders)
			values resulting in a smaller or bigger file depending on selected values.
		`),
		Example: "$ godl video encode video-file.mp4 --vcoded H.265 [ --crf 23 | --output output-name ]",
		Args: func(cmd *cobra.Command, args []string) error {
			if err := cobra.ExactArgs(1)(cmd, args); err != nil {
				return err
			}
			realCmd.FilePath = args[0]
			if err := cmdutil.CanonicalizePaths(&realCmd.FilePath); err != nil {
				return errors.New("failed to canonicalize file paths")
			}
			if f.IgnoreValidations {
				return nil
			}
			return realCmd.Validate()
		},
		Run: func(cmd *cobra.Command, _ []string) {
			runner.RunCommand(cmd, f, realCmd)
		},
	}

	cmd.Flags().StringVarP(&realCmd.Output, "output", "o", "", "The output filename")
	cmd.Flags().Var(&realCmd.VideoCodec, "vcodec", fmt.Sprintf("the video codec. Options: [%s]", strings.Join(types.VideoCodecs().Strings(), ", ")))
	cmd.RegisterFlagCompletionFunc("vcodec", func(_ *cobra.Command, _ []string, _ string) ([]string, cobra.ShellCompDirective) {
		return types.VideoCodecs().Strings(), cobra.ShellCompDirectiveDefault
	})

	cmd.Flags().IntVar(&realCmd.Crf, "crf", 23, heredoc.Doc(`
		The range of the CRF scale is 0–51, where 0 is lossless, 23 is the default, and 51 is worst quality possible.
		A lower value generally leads to higher quality, and a subjectively sane range is 17–28.
		Consider 17 or 18 to be visually lossless or nearly so; it should look the same or nearly the same as the input but it isn't technically lossless.
		The range is exponential, so increasing the CRF value +6 results in roughly half the bitrate / file size, while -6 leads to roughly twice the bitrate.
	`))
	cmd.Flags().BoolVar(&realCmd.IgnoreFileValidations, "ignore-file-validations", false, "Ignore any file type validations")

	return cmd
}
