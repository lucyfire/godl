package encode_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmd/video/encode"
	"gitlab.com/lucyfire/godl/pkg/cmdutil/flags"
	"gitlab.com/lucyfire/godl/pkg/types"
)

var _ = Describe("Encode Command", func() {
	command := encode.Command{
		FilePath:   "some-file.mp4",
		VideoCodec: flags.VideoCodec(types.H265),
		Crf:        23,
	}

	It("should correctly generate cli command", func() {
		Expect(command.DockerCommand().String()).To(Equal("ffmpeg -i some-file.mp4 -vcodec libx265 -crf 23 some-file-encoded.mp4"))
		command.Output = "output-file"
		Expect(command.DockerCommand().String()).To(Equal("ffmpeg -i some-file.mp4 -vcodec libx265 -crf 23 output-file.mp4"))
	})
})
