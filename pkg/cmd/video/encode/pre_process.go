package encode

import (
	"errors"

	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

func (c *Command) Validate() error {
	if err := c.ValidateFiles(); err != nil {
		return err
	}
	if c.Crf > 51 || c.Crf < 0 {
		return errors.New("crf value must be between 0 and 51")
	}
	return nil
}

func (c *Command) ValidateFiles() error {
	if c.IgnoreFileValidations {
		return nil
	}
	if isVideo, err := cmdutil.IsFileVideo(c.FilePath); err != nil || !isVideo {
		return errors.New("file provided isn't in a video format")
	}
	return nil
}
