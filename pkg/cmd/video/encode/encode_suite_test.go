package encode_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestEncodeCommand(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Encode Command Suite")
}
