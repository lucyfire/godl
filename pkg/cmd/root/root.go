package root

import (
	"github.com/MakeNowJust/heredoc"
	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/docker"
	"gitlab.com/lucyfire/godl/pkg/cmd/audio"
	"gitlab.com/lucyfire/godl/pkg/cmd/combine"
	"gitlab.com/lucyfire/godl/pkg/cmd/convert"
	"gitlab.com/lucyfire/godl/pkg/cmd/slice"
	"gitlab.com/lucyfire/godl/pkg/cmd/spotify"
	"gitlab.com/lucyfire/godl/pkg/cmd/video"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"go.uber.org/zap"
)

var (
	Version       string
	YtDlpVersion  string
	SpotDlVersion string
)

func NewCmdRoot(f *cmdutil.Factory) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "godl <command> <subcommand> [flags]",
		Short: "Go operations CLI",
		Long:  `Pre-defined operations for "yt-dlp", "ffmpeg", "eyeD3" and "spotdl"`,
		Version: heredoc.Docf(`
			%s
			Yt-dlp: %s
			Spot-dl: %s
			Image: %s
		`, Version, YtDlpVersion, SpotDlVersion, cmdutil.DockerImage),
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			if f.Debug {
				f.Atom.SetLevel(zap.DebugLevel)
			}
			w, err := docker.NewWrapper(docker.WithLogger(f.Logger))
			f.Docker = w
			return err
		},
	}

	cmd.CompletionOptions.HiddenDefaultCmd = true
	cmd.PersistentFlags().Bool("help", false, "Show help for command")
	cmd.PersistentFlags().BoolVarP(&f.Daemon, "daemon", "d", false, "Run command in daemon")
	cmd.PersistentFlags().BoolVar(&f.Debug, "debug", false, "Displays verbose information")
	cmd.PersistentFlags().BoolVar(&f.IgnoreValidations, "ignore-validations", false, "Ignore command validations")

	cmdutil.AddGroup(cmd, "Core commands",
		audio.NewCmdAudio(f),
		spotify.NewCmdSpotify(f),
		video.NewCmdVideo(f),
		combine.NewCmdCombine(f),
		convert.NewCmdConvert(f),
		slice.NewCmdSlice(f),
	)
	f.AddAnnotationFlags(cmd)
	return cmd
}
