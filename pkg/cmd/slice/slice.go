package slice

import (
	"errors"

	"github.com/spf13/cobra"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/runner"
)

func NewCmdSlice(f *cmdutil.Factory) *cobra.Command {
	realCmd := &Command{}
	cmd := &cobra.Command{
		Use:     "slice { <audio-file> | <video-file> }",
		Short:   "Slices an audio/video file from the specified time",
		Example: "$ godl slice { video-file.mp4 | audio-file.mp3 } --from 00:00:00.0 --to 00:00:10.5 [--output output-name | --ignore-file-validations ]",
		Args: func(cmd *cobra.Command, args []string) error {
			if err := cobra.ExactArgs(1)(cmd, args); err != nil {
				return err
			}
			realCmd.FilePath = args[0]
			if err := cmdutil.CanonicalizePaths(&realCmd.FilePath); err != nil {
				return errors.New("failed to canonicalize file paths")
			}
			if f.IgnoreValidations {
				return nil
			}
			return realCmd.Validate()
		},
		Run: func(cmd *cobra.Command, _ []string) {
			runner.RunCommand(cmd, f, realCmd)
		},
	}

	cmd.Flags().StringVarP(&realCmd.Output, "output", "o", "", "The output filename")
	cmd.Flags().Var(&realCmd.From, "from", "slice start time (Format: {HH:MM:SS[.MS]})")
	cmd.Flags().Var(&realCmd.To, "to", "slice finish time (Format: {HH:MM:SS[.MS]})")
	cmd.Flags().BoolVar(&realCmd.IgnoreFileValidations, "ignore-file-validations", false, "Ignore any file type validations")
	cmd.MarkFlagRequired("from")
	cmd.MarkFlagRequired("to")
	return cmd
}
