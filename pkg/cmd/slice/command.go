package slice

import (
	"path/filepath"
	"strings"

	"gitlab.com/lucyfire/docker"
	"gitlab.com/lucyfire/godl/pkg/cmdutil"
	"gitlab.com/lucyfire/godl/pkg/cmdutil/flags"
)

type Command struct {
	Output                string
	FilePath              string
	From                  flags.Time
	To                    flags.Time
	IgnoreFileValidations bool
}

func (c *Command) ContainerConfig() docker.ContainerConfig {
	return docker.ContainerConfig{
		Image:       cmdutil.DockerImage,
		Labels:      map[string]string{"creator": "godl"},
		Directories: cmdutil.PathsToMounts(c.FilePath),
		Command:     c.DockerCommand(),
		AutoRemove:  true,
	}
}

func (c *Command) DockerCommand() docker.Command {
	basename := filepath.Base(c.FilePath)
	if c.Output == "" {
		c.Output = strings.TrimSuffix(basename, filepath.Ext(basename)) + "-sliced"
	}
	return docker.Command{
		"ffmpeg",
		"-i", c.FilePath,
		"-fps_mode", "vfr",
		"-ss", c.From.String(),
		"-to", c.To.String(),
		c.Output + filepath.Ext(basename),
	}
}
