package slice_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/lucyfire/godl/pkg/cmd/slice"
)

var _ = Describe("Slice Command", func() {
	var command slice.Command
	BeforeEach(func() {
		command = slice.Command{
			FilePath: "some-file.mp3",
		}
		command.From.Set("00:00:00.5")
		command.To.Set("00:01:00.5")
	})

	It("should correctly generate cli command", func() {
		Expect(command.DockerCommand().String()).To(Equal("ffmpeg -i some-file.mp3 -fps_mode vfr -ss 00:00:00.500 -to 00:01:00.500 some-file-sliced.mp3"))
	})

	It("should correctly generate cli command with filename", func() {
		command.Output = "new-file"
		Expect(command.DockerCommand().String()).To(Equal("ffmpeg -i some-file.mp3 -fps_mode vfr -ss 00:00:00.500 -to 00:01:00.500 new-file.mp3"))
	})
})
