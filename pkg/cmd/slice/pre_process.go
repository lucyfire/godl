package slice

import (
	"errors"

	"gitlab.com/lucyfire/godl/pkg/cmdutil"
)

func (c *Command) Validate() error {
	if c.IgnoreFileValidations {
		return nil
	}
	if isAudio, isVideo, err := cmdutil.IsFileAudioOrVideo(c.FilePath); err != nil || (!isAudio && !isVideo) {
		return errors.New("file should be an audio or video")
	}
	return nil
}
