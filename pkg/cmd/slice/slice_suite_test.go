package slice_test

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestSliceCommand(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Slice Command Suite")
}
