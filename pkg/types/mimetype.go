package types

type Type struct {
	Name  string
	Usage string
}

func newType(name, usage string) Type {
	return Type{Name: name, Usage: usage}
}
