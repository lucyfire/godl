package types

var (
	// Audio MimeTypes

	Mp3  = newType("Mp3", "mp3")
	Aiff = newType("Aiff", "aiff")
	Flac = newType("Flac", "flac")
	Aac  = newType("Aac", "aac")
	Ogg  = newType("Ogg", "pgg")
	Wav  = newType("Wav", "wav")

	// Video MimeTypes

	Mkv  = newType("Mkv", "mkv")
	Mov  = newType("Mov", "mov")
	Webm = newType("Webm", "webm")
	Mpg  = newType("Mpg", "mpg")
	Mp4  = newType("Mp4", "mp4")
	M4v  = newType("M4v", "m4v")
	Avi  = newType("Avi", "avi")
	Wmv  = newType("Wmv", "wmv")
	Flv  = newType("Flv", "flv")

	// Video Codecs

	H265 = newType("H.265", "libx265")
	H264 = newType("H.264", "libx264")
)

func AudioMimeTypes() SliceType {
	return SliceType{Mp3, Aiff, Flac, Aac, Ogg, Wav}
}

func VideoMimeTypes() SliceType {
	return SliceType{Mkv, Mov, Webm, Mpg, Mp4, M4v, Avi, Wmv, Flv}
}

func VideoCodecs() SliceType {
	return SliceType{H264, H265}
}
