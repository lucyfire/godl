package types

import "fmt"

type SliceType []Type

func (st SliceType) Strings() []string {
	var s []string
	for _, t := range st {
		s = append(s, t.Name)
	}
	return s
}

func (st SliceType) MatchByName(s string) (Type, error) {
	for _, t := range st {
		if t.Name == s {
			return t, nil
		}
	}

	return Type{}, fmt.Errorf("can't match %q", s)
}
